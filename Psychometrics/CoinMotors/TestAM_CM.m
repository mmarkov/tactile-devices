function varargout = TestAM_CM(varargin)
% TESTAM_CM MATLAB code for TestAM_CM.fig
%      TESTAM_C2, by itself, creates a new TESTAM_C2 or raises the existing
%      singleton*.
%
%      H = TESTAM_C2 returns the handle to a new TESTAM_C2 or the handle to
%      the existing singleton*.
%
%      TESTAM_CM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTAM_C2.M with the given input arguments.
%
%      TESTAM_CM('Property','Value',...) creates a new TESTAM_C2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TestAM_CM_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TestAM_CM_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TestAM_CM

% Last Modified by GUIDE v2.5 16-May-2019 18:55:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TestAM_CM_OpeningFcn, ...
                   'gui_OutputFcn',  @TestAM_CM_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TestAM_CM is made visible.
function TestAM_CM_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TestAM_CM (see VARARGIN)
pnUD = { 'up', 'down', 'stepSizeUp', 'stepSizeDown', 'stopCriterion', 'stopRule', 'startValue', 'xMin', 'xMax' };
pdvUD = { 1, 2, 10, 5, 'trials', 50, 180, 128, 255 };
pnRF = { 'priorAlphaRange', 'beta', 'gamma', 'lambda', 'stopCriterion', 'stopRule', 'PF', 'meanmode', 'xMin', 'xMax' };
pdvRF = { '0:50', 2, 0.5, 0.02, 'trials', 30, 'PAL_Gumbel', 'mean', 0, 50 }; 
pnPM = { 'priorAlphaRange', 'priorBetaRange', 'priorGammaRange', 'priorLambdaRange', 'stimRange', 'PF', 'numTrials' };
pdvPM = { '0:50', '0.05:0.05:5', 0.5, 0.02, '0:50', 'PAL_Gumbel', 30 };
% set( handles.tableUD, 'Data', [ pnUD' GetFields( PAL_AMUD_setupUD, pnUD, pdvUD ) ] );
% set( handles.tableRF, 'Data', [ pnRF' GetFields( PAL_AMRF_setupRF, pnRF, pdvRF ) ] );
% set( handles.tablePM, 'Data', [ pnPM' GetFields( PAL_AMPM_setupPM, pnPM, pdvPM ) ] );
set( handles.tableUD, 'Data', [ pnUD' pdvUD' ] );
set( handles.tableRF, 'Data', [ pnRF' pdvRF' ] );
set( handles.tablePM, 'Data', [ pnPM' pdvPM' ] );

try
    handles.joystick = vrjoystick( 1 );
catch ME
    msgbox( ME.message, 'Joystick!', 'error' );
    handles.joystick = [];
end

hgui = GUI_2AIFC;
Init2AIFCGui( hgui );
handles.hgui = hgui;
movegui( handles.mainFig, 'northeast' );
handles.hfigsc = [];

if isempty( varargin )
    handles.stim = [];
else
    handles.stim = TactorCM( varargin{ 1 } );
end
% Choose default command line output for TestAM_CM
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TestAM_CM wait for user response (see UIRESUME)
% uiwait(handles.mainFig);

function Init2AIFCGui( hgui )
set( hgui.panSelLeft, 'BackgroundColor', 'k' );
set( hgui.panSelRight, 'BackgroundColor', 'k' );
set( hgui.panSelMiddle, 'BackgroundColor', [ 0.9412    0.9412    0.9412 ] );
movegui( hgui.fig, 'northwest' );

function values = GetFields( S, fnames )
values = cell( length( fnames ), 1 );
for i = 1:length( fnames )
    if strcmp( fnames{ i }, 'PF' )
        values{ i } = func2str( S.( fnames{ i } ) );
    elseif strcmp( fnames{ i }, 'priorAlphaRange' ) || strcmp( fnames{ i }, 'priorBetaRange' ) ...
           || strcmp( fnames{ i }, 'stimRange' )
        tmp = S.( fnames{ i } );
        values{ i } = [ num2str( min( tmp ) ) ':' num2str( tmp( 2 ) - tmp( 1 ) ) ':' num2str( max( tmp ) ) ];
    else
        values{ i } = S.( fnames{ i } );
    end
end
% --- Outputs from this function are returned to the command line.
function varargout = TestAM_CM_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in rbUD.
function rbUD_Callback(hObject, eventdata, handles) %#ok<*INUSD,*DEFNU>
% hObject    handle to rbUD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbUD



function txtFreq_Callback(hObject, eventdata, handles)
% hObject    handle to txtFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtFreq as text
%        str2double(get(hObject,'String')) returns contents of txtFreq as a double


% --- Executes during object creation, after setting all properties.
function txtFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function txtI_Callback(hObject, eventdata, handles)
% hObject    handle to txtI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtI as text
%        str2double(get(hObject,'String')) returns contents of txtI as a double
strI = get( hObject, 'String' );
I = str2num( strI );
N = round( length( I ) / 2 ); Istart = I( N );
DATA = get( handles.tableUD, 'Data' );
DATA{ end - 2, 2 } = Istart; DATA{ end - 1, 2 } = I( 1 ); DATA{ end, 2 } = I( end );
set( handles.tableUD, 'Data', DATA );
DATA = get( handles.tableRF, 'Data' );
DATA{ 1, 2 } = strI;
DATA{ end - 1, 2 } = I( 1 ); DATA{ end, 2 } = I( end );
set( handles.tableRF, 'Data', DATA );
DATA = get( handles.tablePM, 'Data' );
DATA{ 1, 2 } = strI; 
DATA{ 5, 2 } = strI;
set( handles.tablePM, 'Data', DATA );


% --- Executes during object creation, after setting all properties.
function txtI_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtPause_Callback(hObject, eventdata, handles)
% hObject    handle to txtPause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtPause as text
%        str2double(get(hObject,'String')) returns contents of txtPause as a double


% --- Executes during object creation, after setting all properties.
function txtPause_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtPause (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtStimDuration_Callback(hObject, eventdata, handles)
% hObject    handle to txtStimDuration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtStimDuration as text
%        str2double(get(hObject,'String')) returns contents of txtStimDuration as a double


% --- Executes during object creation, after setting all properties.
function txtStimDuration_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtStimDuration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in rbRF.
function rbRF_Callback(hObject, eventdata, handles)
% hObject    handle to rbRF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbRF


% --- Executes on button press in rbPM.
function rbPM_Callback(hObject, eventdata, handles)
% hObject    handle to rbPM (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbPM



function txtAlpha_Callback(hObject, eventdata, handles)
% hObject    handle to txtAlpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtAlpha as text
%        str2double(get(hObject,'String')) returns contents of txtAlpha as a double


% --- Executes during object creation, after setting all properties.
function txtAlpha_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtAlpha (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtBeta_Callback(hObject, eventdata, handles)
% hObject    handle to txtBeta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtBeta as text
%        str2double(get(hObject,'String')) returns contents of txtBeta as a double


% --- Executes during object creation, after setting all properties.
function txtBeta_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtBeta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtGamma_Callback(hObject, eventdata, handles)
% hObject    handle to txtGamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtGamma as text
%        str2double(get(hObject,'String')) returns contents of txtGamma as a double


% --- Executes during object creation, after setting all properties.
function txtGamma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtGamma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtLambda_Callback(hObject, eventdata, handles)
% hObject    handle to txtLambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtLambda as text
%        str2double(get(hObject,'String')) returns contents of txtLambda as a double


% --- Executes during object creation, after setting all properties.
function txtLambda_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtLambda (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in btnStart.
function btnStart_Callback(hObject, eventdata, handles)
% hObject    handle to btnStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isempty( hObject )
    sim = true; 
else
    sim = false; 
end

if ~sim
    InitStim( handles )
    if isempty( handles.stim )
        msgbox( 'Stimulator port has to be specified!', 'Stimulator error', 'error' );
        return
    end
end
trueParams = GetTruePFParams( handles );
method = GetMethod( handles );
switch method
    case 'UD'
        data = get( handles.tableUD, 'Data' );
        SetupAM = @PAL_AMUD_setupUD;
        UpdateAM = @PAL_AMUD_updateUD;
        AnalyzeAM = @PAL_AMUD_analyzeUD;
    case 'RF'
        data = get( handles.tableRF, 'Data' );
        SetupAM = @PAL_AMRF_setupRF;
        UpdateAM = @PAL_AMRF_updateRF;
        AnalyzeAM = @( AM ) PlotPDF( AM );
    case 'PM'
        data = get( handles.tablePM, 'Data' );
        SetupAM = @PAL_AMPM_setupPM;
        UpdateAM = @PAL_AMPM_updatePM;
        AnalyzeAM = @( AM ) AM.threshold( end );
end
AM = InitAM( data, SetupAM );
hfig = figure; haxes = axes( 'Parent', hfig ); 
set( hfig, 'Name', 'STAIRCASE SEQUENCE' );
handles.hfigsc = hfig; guidata( handles.mainFig, handles );
stimRange = str2num( get( handles.txtI, 'String' ) );
figure( handles.hgui.fig );
while ~AM.stop
    if sim
        response = rand( 1 ) < PAL_Gumbel( trueParams, AM.xCurrent );
    else
        response = GetResp2AIFC( AM.xCurrent, handles );
    end
    if nargin( UpdateAM ) == 2 || nargin( UpdateAM ) < 0
        AM = UpdateAM( AM, response );
    else
        AM = UpdateAM( AM, AM.xCurrent, response );
    end
    n = 1:length( AM.x ); mask = ( AM.response == 1 );
    hold( haxes, 'off' ), plot( n( mask ), AM.x( mask ), 'o', 'MarkerEdgeColor', 'g', 'MarkerFaceColor', 'g', 'Parent', haxes );
    hold( haxes, 'on' ), plot( n( ~mask ), AM.x( ~mask ), 'o', 'MarkerEdgeColor', 'b', 'MarkerFaceColor', 'b', 'Parent', haxes );
    plot( n, AM.x, 'Parent', haxes );
    ylim( haxes, [ stimRange( 1 ) stimRange( end ) ] );
    title( haxes, 'STAIRCASE SEQUENCE' ); xlabel( haxes, 'TRIAL NUMBER' ); ylabel( haxes, 'AMPLITUDE' );
    drawnow
end
clc
disp( 'STARTING' );
TH = AnalyzeAM( AM );
xlim( haxes, [ 0 length( AM.x ) ] );
line( [ 0 length( AM.x ) ], [ TH TH ], 'LineStyle', '--', 'Color', 'r', 'LineWidth', 2, 'Parent', haxes );
text( 1, TH, num2str( TH ), 'BackgroundColor', 'w', 'FontSize', 12, 'Parent', haxes );
figure( get( haxes, 'Parent' ) );
disp( 'END' );
if ~sim
    channel = get( handles.cboChannels, 'Value' );
    handles.stim.TurnOff(channel);
    handles.stim.Disconnect;
end


function TH = PlotPDF( RF )
% figure
% plot( RF.priorAlphaRange, RF.pdf );
TH = RF.mean;

function resp = GetResp2AIFC( xCurrent, handles )

I = str2num( get( handles.txtI, 'String' ) );
if rand( 1 ) > 0.5, amp1 = xCurrent; amp2 = I( 1 ); else amp1 = I( 1 ); amp2 = xCurrent; end
hgui = handles.hgui; 
ResetGUI( hgui, 'Dr�cke den Knopf um fortzufahren' ); WaitForButtonPress( handles.joystick );
ResetGUI( hgui, 'Mach dich bereit' ); pause( 2 ); 
channel = get( handles.cboChannels, 'Value' );
stimDuration = str2double( get( handles.txtStimDuration, 'String' ) );
pauseDuration = str2double( get( handles.txtPause, 'String' ) );

noise_data = rand(10500,1) - 0.5;
white_noise = audioplayer(noise_data, 8192);
play(white_noise);
pause(0.8)
set( hgui.lblMsg, 'String', 'Erster Stimulus' );
set( [ hgui.panSelLeft, hgui.panStim1, hgui.lblOne ], 'BackgroundColor', 'r' );

if ~isempty( handles.stim )
    amplVector = zeros(1,4);
    amplVector(channel) = amp1;
    handles.stim.SetGain( amplVector )
    handles.stim.TurnOn(channel);
end
pause( stimDuration );
handles.stim.TurnOff(channel);
ResetGUI( hgui, 'Pause' );
pause( pauseDuration )
play(white_noise);
pause(0.8)
set( hgui.lblMsg, 'String', 'Zweiter Stimulus' );
set( [ hgui.panSelRight, hgui.panStim2, hgui.lblTwo ], 'BackgroundColor', 'r' );
if ~isempty( handles.stim )
    amplVector = zeros(1,4);
    amplVector(channel) = amp2;
    handles.stim.SetGain( amplVector )
    handles.stim.TurnOn(channel);
end
pause( stimDuration );
handles.stim.TurnOff(channel);
ResetGUI( hgui, 'W�hle den Stimulus mit der h�heren INTENSIT�T' );
stop = false; c = [ 0.9412    0.9412    0.9412 ]; resp = 0;
if ~isempty( handles.joystick )
    while stop ~= true
        b = button( handles.joystick );
        a = axis( handles.joystick );
        stop = b( 1 );
        if a( 1 ) > 0.5
            if amp2 > amp1, resp = 1; else resp = 0; end
            set( hgui.panSelRight, 'BackgroundColor', 'g' );
            set( hgui.panSelLeft, 'BackgroundColor', 'k' );
            set( hgui.panSelMiddle, 'BackgroundColor', c );
        elseif a( 1 ) < -0.5
            if amp1 > amp2, resp = 1; else resp = 0; end
            set( hgui.panSelLeft, 'BackgroundColor', 'g' );
            set( hgui.panSelRight, 'BackgroundColor', 'k' );
            set( hgui.panSelMiddle, 'BackgroundColor', c );
        else
            stop = false;
            set( hgui.panSelMiddle, 'BackgroundColor', 'g' );
            set( hgui.panSelLeft, 'BackgroundColor', 'k' );
            set( hgui.panSelRight, 'BackgroundColor', 'k' );
        end
        drawnow
    end
end
ResetGUI( hgui, 'Get Ready' );

function WaitForButtonPress( joystick )
if ~isempty( joystick )
    b = button( joystick );
    while b( 1 ), b = button( joystick ); drawnow; end
    while ~b( 1 ), b = button( joystick ); drawnow; end
end

function ResetGUI( hgui, msg )
set( hgui.lblMsg, 'String', msg );
set( [ hgui.panStim1, hgui.lblOne, hgui.panStim2, hgui.lblTwo ], 'BackgroundColor', 'k' );
set( [ hgui.panSelLeft, hgui.panSelRight ], 'BackgroundColor', 'k' );

function AM = InitAM( data, SetupAM )
AM = SetupAM();
for i = 1:size( data, 1 )
    p = data{ i, 1 }; v = data{ i, 2 };
    if ischar( v ),
        tmp = str2num( v ); %#ok<*ST2NM>
        if ~isempty( tmp ), v = tmp; end
    end 
    if strcmp( p, 'PF' ), v = str2func( v ); end
    AM = SetupAM( AM, p, v );
end

function trueParams = GetTruePFParams( handles )
alpha = str2double( get( handles.txtAlpha, 'String' ) );
beta = str2double( get( handles.txtBeta, 'String' ) );
gamma = str2double( get( handles.txtGamma, 'String' ) );
lambda = str2double( get( handles.txtLambda, 'String' ) );
trueParams = [ alpha, beta, gamma, lambda ];

function method = GetMethod( handles )
if get( handles.rbUD, 'Value' )
    method = 'UD';
elseif get( handles.rbRF, 'Value' )
    method = 'RF';
elseif get( handles.rbPM, 'Value' )
    method = 'PM';
end

% --- Executes when selected object is changed in panMethods.
function panMethods_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panMethods 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
if get( handles.rbUD, 'Value' )
    set( handles.tableUD, 'Enable', 'on' )
    set( [ handles.tableRF handles.tablePM ], 'Enable', 'off' );
elseif get( handles.rbRF, 'Value' )
    set( handles.tableRF, 'Enable', 'on' )
    set( [ handles.tableUD handles.tablePM ], 'Enable', 'off' );
else
    set( handles.tablePM, 'Enable', 'on' )
    set( [ handles.tableUD handles.tableRF ], 'Enable', 'off' );
end


% --- Executes during object creation, after setting all properties.
function panMethods_CreateFcn(hObject, eventdata, handles)
% hObject    handle to panMethods (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in btnSim.
function btnSim_Callback(hObject, eventdata, handles)
% hObject    handle to btnSim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
btnStart_Callback( [], [], handles )

function InitStim( handles )
if ~isobject( handles.stim ) || ~isvalid( handles.stim ), return, end
channel = get( handles.cboChannels, 'Value' );

f = str2double( get( handles.txtFreq, 'String' ) );
AMP = str2num( get( handles.txtI, 'String' ) ); 
amp = AMP( 1 );
stimDuration = str2double( get( handles.txtStimDuration, 'String' ) );
stimDuration = stimDuration * 1000;
n = stimDuration * f;
try
    handles.stim.Connect;   
catch ME
    msgbox( ME.message, 'Stimulator interface', 'error!' );
end

% --- Executes on selection change in cboChannels.
function cboChannels_Callback(hObject, eventdata, handles)
% hObject    handle to cboChannels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns cboChannels contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cboChannels


% --- Executes during object creation, after setting all properties.
function cboChannels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cboChannels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close mainFig.
function mainFig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to mainFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isempty( handles.joystick ), close( handles.joystick ); end
delete( handles.hgui.fig );
if ~isempty( handles.stim ), handles.stim.Disconnect; delete( handles.stim ); end
if ishandle( handles.hfigsc ), delete( handles.hfigsc ); end
% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes during object creation, after setting all properties.
function mainFig_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mainFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function tableUD_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tableUD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function mainFig_WindowButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to mainFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
