function varargout = GUI_2AIFC(varargin)
% GUI_2AIFC MATLAB code for GUI_2AIFC.fig
%      GUI_2AIFC, by itself, creates a new GUI_2AIFC or raises the existing
%      singleton*.
%
%      H = GUI_2AIFC returns the handle to a new GUI_2AIFC or the handle to
%      the existing singleton*.
%
%      GUI_2AIFC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_2AIFC.M with the given input arguments.
%
%      GUI_2AIFC('Property','Value',...) creates a new GUI_2AIFC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_2AIFC_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_2AIFC_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_2AIFC

% Last Modified by GUIDE v2.5 22-Apr-2013 13:52:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_2AIFC_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_2AIFC_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_2AIFC is made visible.
function GUI_2AIFC_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_2AIFC (see VARARGIN)

% Choose default command line output for GUI_2AIFC
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI_2AIFC wait for user response (see UIRESUME)
% uiwait(handles.fig);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_2AIFC_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles;
