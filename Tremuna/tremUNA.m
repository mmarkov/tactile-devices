classdef tremUNA < handle
    %tremUNA: Helper class for the control of TremUNA stimulator
    %   Functions for connecting to, programming and activating TremUNA
    
    properties (Constant = true, GetAccess = private)
        baudRate = 500000;
        stopBits = 1;
        dataBits = 8;
        flowControl = 'none';
        parity = 'none'
        writeDelay = 0.001;
    end
    properties (Constant = true)
        NUM_CHANNELS = 8;
        MAX_FREQ = 255;
        MIN_FREQ = 1;
        MAX_PW = 1000;
        MIN_PW = 50;
        MAX_AMP = 50;
        MIN_AMP = 0;
        MIN_DELAY = 0;
        MAX_DELAY = 10000;
        MIN_NUMPULSES = 0;
        MAX_NUMPULSES = 10000;
    end
    properties (SetAccess = private, GetAccess = private)
        cmdON = uint8( '>ON<' );
        cmdOFF = uint8( '>OFF<' );
        cmdC = uint8( '>Cn;c<' );
        cmdF = uint8( '>Fn;f<' );
        cmdW = uint8( '>Wn;HL<' );
        cmdSA = uint8( '>SA;x<' );
        cmdN = uint8( '>Nn;HL<' );
        cmdD = uint8( '>Dn;HL<' );
        cmdSC = uint8( '>SC;12345678<' );
        cmdSF = uint8( '>SF;12345678' );
        cmdSW = uint8( '>SW;1122334455667788<' );
        cmdSP = uint8 ('>SP;cfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNN<');
        states = uint8( 0 );
    end
    properties (SetAccess = private)
        port = [];
        portName = 'COM4';
    end
        
    methods
        function obj = tremUNA( portName )
            obj.portName = portName;
        end
        function Connect( obj )
            obj.port = serial( obj.portName, 'BaudRate', obj.baudRate, 'StopBits', obj.stopBits, ...
                'DataBits', obj.dataBits, 'FlowControl', obj.flowControl, 'Parity', ...
                obj.parity);
            fopen( obj.port );
        end
        function Disconnect( obj )
            fclose( obj.port );
            delete( obj.port );
        end
        function TurnON( obj )
            obj.SendCommand( obj.cmdON );
        end
        function TurnOFF( obj )
            obj.SendCommand( obj.cmdOFF );
        end
        function SetChannelParams( obj, channel, freq, amp, pw, numPulses, delay )
            obj.SetAmp( channel, amp );
            obj.SetPW( channel, pw );
            obj.SetFreq( channel, freq );
            obj.SetNumOfPulses( channel, numPulses );
            obj.SetDelay( channel, delay );
        end
        function SetChannelParamsFromVector( obj, channel, chParams )
            obj.SetChannelParams( channel, chParams( 1 ), chParams( 2 ), chParams( 3 ), ...
                                           chParams( 4 ), chParams( 5 ) );
        end
        function SetAmp( obj, channel, amp )
            obj.cmdC( 3 ) = channel; obj.cmdC( 5 ) = amp;
            obj.SendCommand( obj.cmdC );
        end
        function SetAmpAll( obj, AMP )
            % cmdSC = uint8( '>SC;12345678<' );
            obj.cmdSC( 5:12 ) = AMP;
            obj.SendCommand( obj.cmdSC );
        end
        function SetFreq( obj, channel, freq )
            obj.cmdF( 3 ) = channel; obj.cmdF( 5 ) = freq;
            obj.SendCommand( obj.cmdF );
        end
        function SetFreqAll( obj, FREQ )
            obj.cmdSF( 5:12 ) = FREQ;
            obj.SendCommand( obj.cmdSF );
        end
        function SetPW( obj, channel, pw )
            obj.cmdW( 3 ) = channel; 
            [ high, low ] = tremUNA.GetBytes( pw );
            obj.cmdW( 5 ) = high; obj.cmdW( 6 ) = low;
            obj.SendCommand( obj.cmdW );
        end
        function SetPWAll( obj, PW )
            % cmdSW = uint8( '>SW;1122334455667788<' );
            [ high, low ] = tremUNA.GetBytes( PW );
            obj.cmdSW( 5:2:19 ) = high;
            obj.cmdSW( 6:2:20 ) = low;
            obj.SendCommand( obj.cmdSW );
        end
        % '>SP;cfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNNcfPPDDNN<'
        function SetAll( obj, PARAMS )
            Nbytes = 8;
            params = zeros( 1, Nbytes * obj.NUM_CHANNELS );
            for i = 1:obj.NUM_CHANNELS
                c = PARAMS( i, 1 ); f = PARAMS( i, 2 );
                [ pwh, pwl ] = tremUNA.GetBytes( PARAMS( i, 3 ) );
                [ dh, dl ] = tremUNA.GetBytes( PARAMS( i, 4 ) );
                [ nh, nl ] = tremUNA.GetBytes( PARAMS( i, 5 ) );
                n1 = ( i - 1 )* 8  + 1; n2 = n1 + Nbytes - 1;
                params( n1:n2 ) = [ c f pwh pwl dh dl nh nl ];
            end
            obj.cmdSP( 5:( end - 1 ) ) = params;
            obj.SendCommand( obj.cmdSP );
        end
        function SetNumOfPulses( obj, channel, n )
            obj.cmdN( 3 ) = channel;
            [ high, low ] = tremUNA.GetBytes( n );
            obj.cmdN( 5 ) = high; obj.cmdN( 6 ) = low;
            obj.SendCommand( obj.cmdN );
        end
        function SetDelay( obj, channel, d )
            obj.cmdD( 3 ) = channel; 
            [ high, low ] = tremUNA.GetBytes( d );
            obj.cmdD( 5 ) = high; obj.cmdD( 6 ) = low;
            obj.SendCommand( obj.cmdD );
        end
        function SetStates( obj, states )
            obj.states = bin2dec( states );
            obj.cmdSA( 5 ) = obj.states;
            obj.SendCommand( obj.cmdSA );
        end
        function ChannelON( obj, channel )
            SetChannelState( obj, channel, 1 );
        end
        function ChannelOFF( obj, channel )
            SetChannelState( obj, channel, 0 );
        end
        function SetChannelState( obj, channel, state )
            obj.states = bitset( obj.states, channel, state );
            obj.cmdSA( 5 ) = obj.states;
            obj.SendCommand( obj.cmdSA );
        end
        function AllChannelsOFF( obj )
            obj.SetStates( '00000000' );
        end
        function delete( obj )
            % check if the port object is valid and ,if so, is it open
            if isempty( obj.port ), return, end
            if isvalid(obj.port)
                if ~isempty( obj.port ) && strcmp( obj.port.Status, 'open' )
                    fclose( obj.port );
                end
                delete( obj.port );
            end
        end
    end
    methods (Static, Access = private)
        function [ high, low ] = GetBytes( w )
            low = bitand( uint16( w ), hex2dec( 'FF' ) );
            high = bitshift( bitand( uint16( w ), hex2dec( 'FF00' ) ), -8 );
        end
    end
    methods (Access = private)
        function SendCommand( obj, cmd )
            fwrite( obj.port, cmd );
            pause( obj.writeDelay );
        end
    end
    methods (Static)
        function cmd = CreateSWCommand( PW )
            cmd = uint8( '>SW;1122334455667788<' );
            [ high, low ] = tremUNA.GetBytes( PW );
            cmd( 5:2:19 ) = high;
            cmd( 6:2:20 ) = low;
        end
    end
end

