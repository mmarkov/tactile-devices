function varargout = TestStim(varargin)
% TESTSTIM MATLAB code for TestStim.fig
%      TESTSTIM, by itself, creates a new TESTSTIM or raises the existing
%      singleton*.
%
%      H = TESTSTIM returns the handle to a new TESTSTIM or the handle to
%      the existing singleton*.
%
%      TESTSTIM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTSTIM.M with the given input arguments.
%
%      TESTSTIM('Property','Value',...) creates a new TESTSTIM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TestStim_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TestStim_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TestStim

% Last Modified by GUIDE v2.5 21-Feb-2012 12:02:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TestStim_OpeningFcn, ...
                   'gui_OutputFcn',  @TestStim_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TestStim is made visible.
function TestStim_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TestStim (see VARARGIN)

% Choose default command line output for TestStim
handles.output = hObject;

portNumber = varargin{ 1 };
chParams = [ 100 5 200 0 0 ];
stimParams = [ chParams; chParams; chParams; chParams ];
stimParams = [ stimParams; chParams; chParams; chParams; chParams ];

handles.stimParams = stimParams;
stim = Stimulator( portNumber );
handles.stim = stim;
set( handles.tblStimParams, 'Data', stimParams );
handles.rbCH = [ handles.rbCH1 handles.rbCH2 handles.rbCH3 handles.rbCH4 ...
                 handles.rbCH5 handles.rbCH6 handles.rbCH7 handles.rbCH8 ];

InitStim( stim, stimParams );

UpdateControls( handles );

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TestStim wait for user response (see UIRESUME)
% uiwait(handles.fig);

function InitStim( stim, stimParams )

stim.Connect;
stim.TurnON;
stim.AllChannelsOFF;
for i = 1:size( stimParams, 1 )
    % SetChannelParams( obj, channel, chParams )
    stim.SetChannelParamsFromVector( i, stimParams( i, : ) );
end

% --- Outputs from this function are returned to the command line.
function varargout = TestStim_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txtFreq_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to txtFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtFreq as text
%        str2double(get(hObject,'String')) returns contents of txtFreq as a double
value = str2double( get( hObject, 'String' ) );
f = CheckLimits( value, handles.stim.MAX_FREQ, handles.stim.MIN_FREQ );
set( hObject, 'String', num2str( f ) );
ch = GetActiveChannel( handles );
handles.stim.SetFreq( ch, f );
handles.stimParams( ch, 1 ) = f;
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtFreqUp.
function txtFreqUp_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to txtFreqUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtFreq, 'String' ) );
set( handles.txtFreq, 'String', num2str( value + 1 ) );
txtFreq_Callback(handles.txtFreq, eventdata, handles)

% --- Executes on button press in txtFreqDown.
function txtFreqDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtFreqDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtFreq, 'String' ) );
set( handles.txtFreq, 'String', num2str( value - 1 ) );
txtFreq_Callback(handles.txtFreq, eventdata, handles)


function txtAmp_Callback(hObject, eventdata, handles)
% hObject    handle to txtAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtAmp as text
%        str2double(get(hObject,'String')) returns contents of txtAmp as a double
value = str2double( get( hObject, 'String' ) );
I = CheckLimits( value, handles.stim.MAX_AMP, handles.stim.MIN_AMP );
set( hObject, 'String', num2str( I ) );
ch = GetActiveChannel( handles );
handles.stim.SetAmp( ch, I );
handles.stimParams( ch, 2 ) = I;
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtAmp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtAmpUp.
function txtAmpUp_Callback(hObject, eventdata, handles)
% hObject    handle to txtAmpUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtAmp, 'String' ) );
set( handles.txtAmp, 'String', num2str( value + 1 ) );
txtAmp_Callback(handles.txtAmp, eventdata, handles)

% --- Executes on button press in txtAmpDown.
function txtAmpDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtAmpDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtAmp, 'String' ) );
set( handles.txtAmp, 'String', num2str( value - 1 ) );
txtAmp_Callback(handles.txtAmp, eventdata, handles)


function txtPW_Callback(hObject, eventdata, handles)
% hObject    handle to txtPW (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtPW as text
%        str2double(get(hObject,'String')) returns contents of txtPW as a double
value = str2double( get( hObject, 'String' ) );
pw = CheckLimits( value, handles.stim.MAX_PW, handles.stim.MIN_PW );
set( hObject, 'String', num2str( pw ) );
ch = GetActiveChannel( handles );
handles.stim.SetPW( ch, pw );
handles.stimParams( ch, 3 ) = pw;
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtPW_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtPW (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtPWUp.
function txtPWUp_Callback(hObject, eventdata, handles)
% hObject    handle to txtPWUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtPW, 'String' ) );
set( handles.txtPW, 'String', num2str( value + 10 ) );
txtPW_Callback(handles.txtPW, eventdata, handles)

% --- Executes on button press in txtPWDown.
function txtPWDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtPWDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtPW, 'String' ) );
set( handles.txtPW, 'String', num2str( value - 10 ) );
txtPW_Callback(handles.txtPW, eventdata, handles)


function txtNumPulses_Callback(hObject, eventdata, handles)
% hObject    handle to txtNumPulses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtNumPulses as text
%        str2double(get(hObject,'String')) returns contents of txtNumPulses as a double
value = str2double( get( hObject, 'String' ) );
numPulses = CheckLimits( value, handles.stim.MAX_NUMPULSES, handles.stim.MIN_NUMPULSES );
set( hObject, 'String', num2str( numPulses ) );
ch = GetActiveChannel( handles );
handles.stim.SetNumOfPulses( ch, numPulses );
handles.stimParams( ch, 4 ) = numPulses;
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtNumPulses_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtNumPulses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtNumPulsesUp.
function txtNumPulsesUp_Callback(hObject, eventdata, handles)
% hObject    handle to txtNumPulsesUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtNumPulses, 'String' ) );
set( handles.txtNumPulses, 'String', num2str( value + 100 ) );
txtNumPulses_Callback(handles.txtNumPulses, eventdata, handles)

% --- Executes on button press in txtNumPulsesDown.
function txtNumPulsesDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtNumPulsesDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtNumPulses, 'String' ) );
set( handles.txtNumPulses, 'String', num2str( value - 100 ) );
txtNumPulses_Callback(handles.txtNumPulses, eventdata, handles)


function txtDelay_Callback(hObject, eventdata, handles)
% hObject    handle to txtDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtDelay as text
%        str2double(get(hObject,'String')) returns contents of txtDelay as a double
value = str2double( get( hObject, 'String' ) );
d = CheckLimits( value, handles.stim.MAX_DELAY, handles.stim.MIN_DELAY );
set( hObject, 'String', num2str( d ) );
ch = GetActiveChannel( handles );
handles.stim.SetDelay( ch, d );
handles.stimParams( ch, 5 ) = d;
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtDelay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtDelayUp.
function txtDelayUp_Callback(hObject, eventdata, handles)
% hObject    handle to txtDelayUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtDelay, 'String' ) );
set( handles.txtDelay, 'String', num2str( value + 100 ) );
txtDelay_Callback(handles.txtDelay, eventdata, handles)

% --- Executes on button press in txtDelayDown.
function txtDelayDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtDelayDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtDelay, 'String' ) );
set( handles.txtDelay, 'String', num2str( value - 100 ) );
txtDelay_Callback(handles.txtDelay, eventdata, handles)

% --- Executes on button press in tbCH1.
function tbCH1_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH1
SetChannelState( hObject, handles.stim, 1, get(hObject,'Value') )

% --- Executes on button press in tbCH2.
function tbCH2_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH2
SetChannelState( hObject, handles.stim, 2, get(hObject,'Value') )

% --- Executes on button press in tbCH3.
function tbCH3_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH3
SetChannelState( hObject, handles.stim, 3, get(hObject,'Value') )

% --- Executes on button press in tbCH4.
function tbCH4_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH4
SetChannelState( hObject, handles.stim, 4, get(hObject,'Value') )

% --- Executes on button press in tbCH5.
function tbCH5_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH5
SetChannelState( hObject, handles.stim, 5, get(hObject,'Value') )

% --- Executes on button press in tbCH6.
function tbCH6_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH6
SetChannelState( hObject, handles.stim, 6, get(hObject,'Value') )

% --- Executes on button press in tbCH7.
function tbCH7_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH7
SetChannelState( hObject, handles.stim, 7, get(hObject,'Value') )

% --- Executes on button press in tbCH8.
function tbCH8_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH8
SetChannelState( hObject, handles.stim, 8, get(hObject,'Value') )

% --- Executes when user attempts to close fig.
function fig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to fig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete( handles.stim );

% Hint: delete(hObject) closes the figure
delete(hObject);

function ch = GetActiveChannel( handles )

channels = 1:8;
H = [ handles.rbCH1 handles.rbCH2 handles.rbCH3 handles.rbCH4 ...
      handles.rbCH5 handles.rbCH6 handles.rbCH7 handles.rbCH8 ];
ch = channels( logical( cell2mat( get( H, 'Value' ) )' ) );

function val = CheckLimits( val, maxVal, minVal )
if val > maxVal, val = maxVal; end
if val < minVal, val = minVal; end


% --- Executes during object creation, after setting all properties.
function pnlStimParams_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pnlStimParams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes when selected object is changed in pnlStimParams.
function pnlStimParams_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in pnlStimParams 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
UpdateControls( handles )

function SetChannelState( hObject, stim, channel, state )
handles = guidata( hObject );
if state == 1, set( handles.rbCH( channel ), 'Value', state ), UpdateControls( handles ); end
state = get( hObject, 'Value' );
stim.SetChannelState( channel, state );
if state, c = [ 0 1 0 ]; else c = [ 1 0 0 ]; end
set( hObject, 'BackgroundColor', c );

function UpdateControls( handles )
ch = GetActiveChannel( handles );
set( handles.txtFreq, 'String', num2str( handles.stimParams( ch, 1 ) ) );
set( handles.txtAmp, 'String', num2str( handles.stimParams( ch, 2 ) ) );
set( handles.txtPW, 'String', num2str( handles.stimParams( ch, 3 ) ) );
set( handles.txtNumPulses, 'String', num2str( handles.stimParams( ch, 4 ) ) );
set( handles.txtDelay, 'String', num2str( handles.stimParams( ch, 5 ) ) );
