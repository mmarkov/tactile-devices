function varargout = PsychoTest(varargin)
% PSYCHOTEST MATLAB code for PsychoTest.fig
%      PSYCHOTEST, by itself, creates a new PSYCHOTEST or raises the existing
%      singleton*.
%
%      H = PSYCHOTEST returns the handle to a new PSYCHOTEST or the handle to
%      the existing singleton*.
%
%      PSYCHOTEST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PSYCHOTEST.M with the given input arguments.
%
%      PSYCHOTEST('Property','Value',...) creates a new PSYCHOTEST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PsychoTest_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PsychoTest_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PsychoTest

% Last Modified by GUIDE v2.5 19-May-2015 15:38:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PsychoTest_OpeningFcn, ...
                   'gui_OutputFcn',  @PsychoTest_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PsychoTest is made visible.
function PsychoTest_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PsychoTest (see VARARGIN)

nClasses = str2double( get( handles.txtNumClasses, 'String' ) );
nCols = size( get( handles.tableClasses, 'ColumnName' ), 1 );
set( handles.tableClasses, 'Data', MakeCell( nClasses, nCols ) );
try
    handles.stim = Stimulator( 'COM3' );
    handles.stim.Connect;
catch ME
    disp( ME.message );
end

% Choose default command line output for PsychoTest
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PsychoTest wait for user response (see UIRESUME)
% uiwait(handles.figure1);

function c = MakeCell( nRows, nCols )
c = cell( nRows, nCols );
for iRow = 1:nRows
    for iCol = 1:nCols
        c{ iRow, iCol } = '';
    end
end

% --- Outputs from this function are returned to the command line.
function varargout = PsychoTest_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txtNumRep_Callback(hObject, eventdata, handles)
% hObject    handle to txtNumRep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtNumRep as text
%        str2double(get(hObject,'String')) returns contents of txtNumRep as a double


% --- Executes during object creation, after setting all properties.
function txtNumRep_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtNumRep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtStimDur_Callback(hObject, eventdata, handles)
% hObject    handle to txtStimDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtStimDur as text
%        str2double(get(hObject,'String')) returns contents of txtStimDur as a double


% --- Executes during object creation, after setting all properties.
function txtStimDur_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtStimDur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnGenerateRand.
function btnGenerateRand_Callback(hObject, eventdata, handles)
% hObject    handle to btnGenerateRand (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
classData = get( handles.tableClasses, 'Data' );
nClass = size( classData, 1 );
nRep = str2double( get( handles.txtNumRep, 'String' ) );
tmp = repmat( 1:nClass, 1, nRep );
order = tmp( randperm( length( tmp ) ) );
pdata = MakeProtocol( order, classData );
set( handles.tableProtocol, 'ColumnFormat', { 'char', 'char', classData( :, end )', 'char' } );
set( handles.tableProtocol, 'Data', pdata );

function pdata = MakeProtocol( order, classData )
pdata = cell( length( order ), 5 );
pdata( 1, : ) = { '0', '0', '0', '0', 'X' };
for i = 2:( length( order ) + 1 )
    iClass = order( i - 1 );
    pdata{ i, 1 } = num2str( iClass );
    pdata{ i, 2 } = classData{ iClass, end };
    pdata{ i, 3 } = classData{ 1, end };
    pdata{ i, 4 } = 1; 
end
pdata{ 1, end } = 'X';

% --- Executes on button press in btnNext.
function btnNext_Callback(hObject, eventdata, handles)
% hObject    handle to btnNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pdata = get( handles.tableProtocol, 'Data' );
I = find( cellfun( @( x ) strcmp( x, 'X' ), pdata( :, end ) ), 1 );
pdata{ I, end } = [];
if I < size( pdata, 1 ), I = I + 1; else I = 1; end 
pdata{ I, end } = 'X';
set( handles.tableProtocol, 'Data', pdata );
if get( handles.chkAutoStim, 'Value' ), PresentStimulus( handles ); end

function PresentStimulus( handles )
pdata = get( handles.tableProtocol, 'Data' );
Iproto = find( cellfun( @( x ) strcmp( x, 'X' ), pdata( :, end ) ), 1 );
Iclass = str2double( pdata{ Iproto, 1 } );
if Iclass ~= 0
    cdata = get( handles.tableClasses, 'Data' );
    classRow = cdata( Iclass, : );
    amp = str2double( classRow{ 1 } ); pw = str2double( classRow{ 2 } );
    freq = str2double( classRow{ 3 } ); channel = str2double( classRow{ 4 } );
    % SetChannelParams( obj, channel, freq, amp, pw, numPulses, delay )
    handles.stim.SetChannelParams( channel, freq, amp, pw, 0, 0 );
    handles.stim.ChannelON( channel );
    dur = str2double( get( handles.txtStimDur, 'String' ) );
    pause( dur );
    handles.stim.ChannelOFF( channel );
end


% --- Executes on selection change in ppmCurrentClass.
function ppmCurrentClass_Callback(hObject, eventdata, handles)
% hObject    handle to ppmCurrentClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ppmCurrentClass contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ppmCurrentClass


% --- Executes during object creation, after setting all properties.
function ppmCurrentClass_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ppmCurrentClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ppmPresentedClass.
function ppmPresentedClass_Callback(hObject, eventdata, handles)
% hObject    handle to ppmPresentedClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ppmPresentedClass contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ppmPresentedClass


% --- Executes during object creation, after setting all properties.
function ppmPresentedClass_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ppmPresentedClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ppmGuessedClass.
function ppmGuessedClass_Callback(hObject, eventdata, handles)
% hObject    handle to ppmGuessedClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ppmGuessedClass contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ppmGuessedClass


% --- Executes during object creation, after setting all properties.
function ppmGuessedClass_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ppmGuessedClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnUpdate.
function btnUpdate_Callback(hObject, eventdata, handles)
% hObject    handle to btnUpdate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in btnPresentClass.
function btnPresentClass_Callback(hObject, eventdata, handles)
% hObject    handle to btnPresentClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
PresentStimulus( handles )


function txtNumClasses_Callback(hObject, eventdata, handles)
% hObject    handle to txtNumClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtNumClasses as text
%        str2double(get(hObject,'String')) returns contents of txtNumClasses as a double
nClasses = str2double(get(hObject,'String'));
nCols = size( get( handles.tableClasses, 'ColumnName' ), 1 );
set( handles.tableClasses, 'Data', MakeCell( nClasses, nCols ) );

% --- Executes during object creation, after setting all properties.
function txtNumClasses_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtNumClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnSaveClasses.
function btnSaveClasses_Callback(hObject, eventdata, handles)
% hObject    handle to btnSaveClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = get( handles.tableClasses, 'Data' ); %#ok<NASGU>
[ fname, pname ] = uiputfile( '*.mat' );
save( fullfile( pname, fname ), 'data' );

% --- Executes on button press in btnLoadClasses.
function btnLoadClasses_Callback(hObject, eventdata, handles)
% hObject    handle to btnLoadClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[ fname, pname ] = uigetfile( '*.mat' );
load( fullfile( pname, fname ) );
set( handles.tableClasses, 'Data', data );


% --- Executes on button press in btnSaveProtocol.
function btnSaveProtocol_Callback(hObject, eventdata, handles)
% hObject    handle to btnSaveProtocol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = get( handles.tableProtocol, 'Data' ); %#ok<NASGU>
[ fname, pname ] = uiputfile( '*.mat' );
save( fullfile( pname, fname ), 'data' );

% --- Executes on button press in btnLoadProtocol.
function btnLoadProtocol_Callback(hObject, eventdata, handles)
% hObject    handle to btnLoadProtocol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[ fname, pname ] = uigetfile( '*.mat' );
load( fullfile( pname, fname ) );
set( handles.tableProtocol, 'Data', data );

% --- Executes on button press in btnPrevClass.
function btnPrevClass_Callback(hObject, eventdata, handles)
% hObject    handle to btnPrevClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pdata = get( handles.tableProtocol, 'Data' );
I = find( cellfun( @( x ) strcmp( x, 'X' ), pdata( :, end ) ), 1 );
pdata{ I, end } = [];
if I > 1, pdata{ I - 1, end } = 'X'; else pdata{ end, end } = 'X'; end
set( handles.tableProtocol, 'Data', pdata );


% --- Executes when entered data in editable cell(s) in tableProtocol.
function tableProtocol_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tableProtocol (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
newClass = eventdata.NewData;
cformat = get( hObject, 'ColumnFormat' );
classNames = cformat{ 3 };
I = find( cellfun( @( x ) strcmp( x, newClass ), classNames ), 1 );
pdata = get( hObject, 'Data' );
pdata{ eventdata.Indices( 1 ), 4 } = I;
set( hObject, 'Data', pdata );

% --- Executes when selected cell(s) is changed in tableProtocol.
function tableProtocol_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to tableProtocol (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in btnTurnStim.
function btnTurnStim_Callback(hObject, eventdata, handles)
% hObject    handle to btnTurnStim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of btnTurnStim
if get( hObject, 'Value' )
    set( hObject, 'BackgroundColor', 'g', 'String', 'Stimulator ON' );
    handles.stim.TurnON;
else
    set( hObject, 'BackgroundColor', 'r', 'String', 'Stimulator OFF' );
    handles.stim.TurnOFF;
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
handles.stim.Disconnect;
delete( handles.stim );


% --- Executes on button press in chkAutoStim.
function chkAutoStim_Callback(hObject, eventdata, handles)
% hObject    handle to chkAutoStim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkAutoStim
if get( hObject, 'Value' )
    set( handles.btnPresentClass, 'Enable', 'off' );
else
    set( handles.btnPresentClass, 'Enable', 'on' );
end


% --- Executes on button press in btnGenerateRegular.
function btnGenerateRegular_Callback(hObject, eventdata, handles)
% hObject    handle to btnGenerateRegular (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
classData = get( handles.tableClasses, 'Data' );
nClass = size( classData, 1 );
nRep = str2double( get( handles.txtNumRep, 'String' ) );
tmp = repmat( 1:nClass, 1, nRep );
pdata = MakeProtocol( tmp, classData );
set( handles.tableProtocol, 'ColumnFormat', { 'char', 'char', classData( :, end )', 'char' } );
set( handles.tableProtocol, 'Data', pdata );
