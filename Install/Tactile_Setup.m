% Remove the CLS from the matlab search path
display('INSTALLING Tactile Devices: Removing previos versions ...')
Tactile_Remove

%% Find the root level dir 
currentDir = mfilename('fullpath');
rootDirIdx = strfind(currentDir,'Install');
rootDir = currentDir(1:rootDirIdx(end)-2);

%% Add each of the dirs to the matlab search path
display('INSTALLING Tactile Devices: Adding directory trees ...')
currentPath = pathdef();
dirs  = {'C2', 'IntFes', 'RehaStim', 'Tremuna', 'Psychometrics', 'TactorsCM'};
installPath = [];
for i = 1:length(dirs)
    folderS = genpath([rootDir, '\', dirs{i}]);
    dirsSeparate =  strsplit(folderS, ';');
    folderS = [];
    for j=1:length(dirsSeparate)
        if ~isempty(dirsSeparate{j}) && isempty(strfind(dirsSeparate{j}, '\Doc'))
           folderS  = [folderS  dirsSeparate{j} ,';'];
        end
    end
    if ~isempty(folderS)
        installPath = [installPath folderS];
        path(folderS,currentPath);
    end
    currentPath = path();
end
path(rootDir,currentPath);
currentPath = path();

%% Save the current search path
display('INSTALLING Tactile Devices: Saving new search path ...')
if savepath
    error('You need administrator privilegies in order to perform this operation');
else
    save([rootDir, '\Install\', 'installPath.mat'], 'installPath')
    display('INSTALLING Tactile Devices: Finished!')
end

%% Clear the variables
clear currentDir fileName rootDirIdx rootDir folderS currentPath installPath dirs i validDirs dirsSeparate j