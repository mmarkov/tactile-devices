function [cmdUpdate]  = RehaStim_Update_ChannelBased(IntV, unmodPval, modP,activeChannels)

%% Initialisation fixed
chNA = activeChannels(activeChannels<5); % Always All channels (1:4)
chNB = activeChannels(activeChannels>4); % Always All channels (5:8)


%%%%%%%%%%%%%%%%%%%%%% Channel Update %%%%%%%%%%%%%%%%%%%%%

PW=zeros(1,size(IntV,2));
C=zeros(1,size(IntV,2));

switch modP
    case 1
        C = IntV;
        PW = repmat(unmodPval,1,size(IntV,2));
    case 2
        PW = IntV;
        C = repmat(unmodPval,1,size(IntV,2));
end
% PW = [180 180 180 180 180 180 180 180]; % pulse width in us;
% C = [4 4 4 4 4 4 4 4]; % Current amplitude in REAL mA;
M = zeros(1,size(PW,2));

% if size(PW,2)~= size([chNA,chNB],2)
%     error('size(PW)~= size([chNA,chNB])');
% end
% if size(C,2)~= size([chNA,chNB],2)
%     error('size(C)~= size([chNA,chNB])');
% end

cmdBitsPre= '1HHCCCCC';
cmdBitsPre(strfind(cmdBitsPre,'HH'):strfind(cmdBitsPre,'HH')+1) = '01'; % protocol update

cmdBits= [cmdBitsPre, repmat(repmat('0',1,size('0MMXXXWW0WWWWWWW0IIIIIII',2)),1,size([chNA,chNB],2))];
CHKch=zeros(1,size([chNA,chNB],2));

for ch=1:size([chNA,chNB],2);
    cmdBits((ch-1)*3*8+9:(ch-1)*3*8+32)= ['0MMXXXWW0WWWWWWW0IIIIIII'];
    
    temp = dec2bin(PW(ch));
    PWB = [repmat('0',[1, 9-length(temp)]) temp];
    cmdBits(cmdBits == 'W') = PWB; % pulse width
    
    temp = dec2bin(C(ch));
    CB = [repmat('0',[1, 7-length(temp)]) temp];
    cmdBits(cmdBits == 'I') = CB; % current intensity
    
    temp = dec2bin(M(ch));
    MB = [repmat('0',[1, 2-length(temp)]) temp];
    cmdBits(cmdBits == 'M') = MB; % mode
    CHKch(ch)=PW(ch)+C(ch)+M(ch);
end

temp = dec2bin(mod(sum(CHKch),32));
CHK = [repmat('0',[1, 5-length(temp)]) temp];
cmdBits(cmdBits == 'C') = CHK; % checksum

cmdBits(cmdBits == 'X') = '0'; % replace... good idea?!
% cmdUpdate=zeros(1,25,'uint8');
% cmdUpdate(1)=bin2dec(cmdBits(1:8));
% cmdUpdate(2)=bin2dec(cmdBits(9:16));
% cmdUpdate(3)=bin2dec(cmdBits(17:24));
% cmdUpdate(4)=bin2dec(cmdBits(25:32));
% cmdUpdate(5)=bin2dec(cmdBits(33:40));
% cmdUpdate(6)=bin2dec(cmdBits(41:48));
% cmdUpdate(7)=bin2dec(cmdBits(49:56));
% cmdUpdate(8)=bin2dec(cmdBits(57:64));
% cmdUpdate(9)=bin2dec(cmdBits(65:72));
% cmdUpdate(10)=bin2dec(cmdBits(73:80));
% cmdUpdate(11)=bin2dec(cmdBits(81:88));
% cmdUpdate(12)=bin2dec(cmdBits(89:96));
% cmdUpdate(13)=bin2dec(cmdBits(97:104));
% cmdUpdate(14)=bin2dec(cmdBits(105:112));
% cmdUpdate(15)=bin2dec(cmdBits(113:120));
% cmdUpdate(16)=bin2dec(cmdBits(121:128));
% cmdUpdate(17)=bin2dec(cmdBits(129:136));
% cmdUpdate(18)=bin2dec(cmdBits(137:144));
% cmdUpdate(19)=bin2dec(cmdBits(145:152));
% cmdUpdate(20)=bin2dec(cmdBits(153:160));
% cmdUpdate(21)=bin2dec(cmdBits(161:168));
% cmdUpdate(22)=bin2dec(cmdBits(169:176));
% cmdUpdate(23)=bin2dec(cmdBits(177:184));
% cmdUpdate(24)=bin2dec(cmdBits(185:192));
% cmdUpdate(25)=bin2dec(cmdBits(193:200));

 for  numCh=1:3*(length(chNA)+length(chNB))+1
 cmdUpdate(numCh)=bin2dec(cmdBits(8*(numCh-1)+1:8*(numCh)));
 end
end