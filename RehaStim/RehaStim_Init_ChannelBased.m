function [cmdInit]  = RehaStim_Init_ChannelBased(freqV,activeChannels)

%% Initialisation fixed
chNA = activeChannels(activeChannels<5); % Always All channels (1:4)
chNB = activeChannels(activeChannels>4); % Always All channels (5:8)
chLf = []; % Always []; Channels with lower frequency (frequency depending on F and N_factor);

ApproxFreq=freqV(1); % in Hz
Main_Time = (1000/ApproxFreq-1)/0.5; % in ms
    Main_Time=round(Main_Time);
     RealFrequency=1/(1+Main_Time*0.5)*1000
%     ts1=1/F;

N_Factor = 1; % Always=1; Parameter N_factor, defines how many times the stimulation is skipped for channels specified in Channel_Lf as well as Channel_Stim (0 = no skip, 1 = skip once, ..., 7 = skip 7 times)
ipT = 1.5* (max(length(chNA),length(chNB))); % Set to lowest number: Time from onset of first pulse to onset of next pulse within a stimulation cycles, in ms
Group_Time = (ipT-1.5)/0.5; % in ms
cmdBits = '1HHCCCFF0FNNNNNN0NNLLLLL0LLLXXGG0GGGTTTT0TTTTTTT';
cmdBits(cmdBits == 'H') = '0'; % protocol

% FB = dec2bin(N_Factor);
FB = [repmat('0',[1, 3-length(dec2bin(N_Factor))]) dec2bin(N_Factor)];
cmdBits(cmdBits == 'F') = FB; % N_factor

Channel_Stim = '00000000';
for ch=[chNA,chNB]
    Channel_Stim(8-ch+1) = '1';
end
cmdBits(cmdBits == 'N') = Channel_Stim; % Active channels

Channel_Lf = '00000000';
for ch=chLf
    Channel_Lf(8-ch+1) = '1';
end
cmdBits(cmdBits == 'L') = Channel_Lf; % Lf channels


%GT = dec2bin(Group_Time);
GT = [repmat('0',[1, 5-length(dec2bin(Group_Time))]) dec2bin(Group_Time)];
cmdBits(cmdBits == 'G') = GT; % group time

%MT = dec2bin(Main_Time);
MT = [repmat('0',[1, 11-length(dec2bin(Main_Time))]) dec2bin(Main_Time)];
cmdBits(cmdBits == 'T') = MT; % main time

%CHK = dec2bin(mod(N_Factor+bin2dec(Channel_Stim)+ bin2dec(Channel_Lf) +Group_Time + Main_Time,8));
CHK = [repmat('0',[1, 3-length(dec2bin(mod(N_Factor+bin2dec(Channel_Stim)+ bin2dec(Channel_Lf) +Group_Time + Main_Time,8)))]) dec2bin(mod(N_Factor+bin2dec(Channel_Stim)+ bin2dec(Channel_Lf) +Group_Time + Main_Time,8))];
cmdBits(cmdBits == 'C') = CHK; % checksum

cmdBits(cmdBits == 'X') = '00'; % really good to do that?!

cmdInit=zeros(1,6,'uint8');
cmdInit(1)=bin2dec(cmdBits(1:8));
cmdInit(2)=bin2dec(cmdBits(9:16));
cmdInit(3)=bin2dec(cmdBits(17:24));
cmdInit(4)=bin2dec(cmdBits(25:32));
cmdInit(5)=bin2dec(cmdBits(33:40));
cmdInit(6)=bin2dec(cmdBits(41:48));% uint8([b1 b2 b3 b4 b5 b6]);


end