classdef StimulatorRehaStim < handle
    %StimulatorRehaStim: Helper class for the control of RehaStim stimulator
    %   Functions for connecting to, programming and activating RehaStim
    
    properties (Constant = true, GetAccess = private)
        baudRate = 115200;
        stopBits = 2;
        dataBits = 8;
        flowControl = 'hardware';
        parity = 'none'
        writeDelay = 0.001; %left the same now for RehaStim
    end
    
    properties (Constant = true)
        NUM_CHANNELS = 8;
        MAX_FREQ = 400;
        MIN_FREQ = 1;
        MAX_PW = 500;
        MIN_PW = 20;
        MAX_AMP = 10;
        MIN_AMP = 0;
        MIN_DELAY = 0;
        MAX_DELAY = 10000; %left the same now for RehaStim
        MIN_NUMPULSES = 0; %left the same now for RehaStim
        MAX_NUMPULSES = 10000; %left the same now for RehaStim
        
    end
    properties (SetAccess = private, GetAccess = private)
        previousAmpAll = [0 0 0 0 0 0 0 0];
        previousPWAll = [0 0 0 0 0 0 0 0];
        previousFreq = 1;
    end
    
    properties (SetAccess = private)
        port = [];
        portName = 'COM8';
    end
    
    properties
        findActiveChannels = [0 0 0 0 0 0 0 0];
        currentAmpAll = [0 0 0 0 0 0 0 0];
        activeChannels = [];    
        currentPWAll = [0 0 0 0 0 0 0 0];
        currentFreq = 1;
    end
    %
    methods
        function obj = StimulatorRehaStim( portName )
            obj.portName = portName;
        end
        
        function Connect( obj )
            obj.port = serial( obj.portName, 'BaudRate', obj.baudRate, 'StopBits', obj.stopBits, ...
                'DataBits', obj.dataBits, 'FlowControl', obj.flowControl, 'Parity', ...
                obj.parity);
            fopen( obj.port );
        end
        
        function Disconnect( obj )
            TurnOFF( obj );
            fclose( obj.port );
            delete( obj.port );
        end
        
        function TurnOFF( obj )
            testfwrite1 = 1;
            tic
            i=0;
            while testfwrite1==1
                try
                    fwrite( obj.port,bin2dec( '11000000' ), 'uint8' );
                    testfwrite1=0;
                catch
                end
                i=i+1;
            end
            i=i;
            t_turnOFF=toc;
            
            
            c = fread(obj.port, 1, 'uint8');
            obj.previousPWAll=[0 0 0 0 0 0 0 0];
            obj.previousAmpAll=[0 0 0 0 0 0 0 0];
            obj.previousFreq=1;
            
            obj.currentPWAll=obj.previousPWAll;
            obj.currentAmpAll=obj.previousAmpAll;
            obj.currentFreq=obj.previousFreq;
        end
        
        function SetChannelState( obj, channel, state)
            obj.findActiveChannels(channel)=state;
            obj.activeChannels=find(obj.findActiveChannels);
            
        end
        
        function ChannelON( obj, channel )
            SetChannelState( obj, channel, 1 );
        end
        function ChannelOFF( obj, channel )
            SetChannelState( obj, channel, 0 );
            SetFreq(obj,obj.currentFreq);
        end
        
        function SetChannelParams( obj, channel, freq, amp, pw)
            obj.SetFreq( freq );
            obj.SetAmp( channel, amp );
            obj.SetPW( channel, pw );
        end
        
        function SetChannelParamsFromVector( obj, channel, chParams )
            obj.SetChannelParams( channel, chParams( 1 ), chParams( 2 ), chParams( 3 )); %chParams( 4 ), chParams( 5 ) );
        end
        
        function SetAmp( obj, channel, Amp )
            if Amp > obj.MAX_AMP
                Amp = 2;
                h = warndlg('The chosen amplitude was too high. It was reset to 2 mA.');
            end
            obj.currentAmpAll=obj.previousAmpAll;
            obj.currentAmpAll(channel)=Amp;
            obj.SendCommand('Amp');
        end
        
        function SetAmpAll( obj, AmpAll )
            %sprintf('%%%%%%%%%Set Amp%%%%%%%%%%%%%')
            
            if AmpAll > obj.MAX_AMP
                AmpAll = 2;
                h = warndlg('The chosen amplitude was too high. It was reset to 2 mA.');     
            end
            obj.currentAmpAll=AmpAll;
            obj.SendCommand('AmpAll');
        end
        
        function [StopPar,InitPar,UpdateCmd]=SetFreq( obj, freq )
            % sprintf('%%%%%%%%%Set Freq%%%%%%%%%%%%%')
            ActiveChannels=obj.activeChannels
            if (size(find(ismember(obj.activeChannels,[1:4])),2)==4 || size(find(ismember(obj.activeChannels,[5:8])),2)==4) && freq>100
                error('Frequency should not higher than 100 Hz if four channels in [1:4] or [5:8] are active!');
            elseif (size(find(ismember(obj.activeChannels,[1:4])),2)==3 || size(find(ismember(obj.activeChannels,[5:8])),2)==3) && freq>133
                error('Frequency should not higher than 133 Hz if three channels in [1:4] or [5:8] are active!');
                
            elseif (size(find(ismember(obj.activeChannels,[1:4])),2)==2 || size(find(ismember(obj.activeChannels,[5:8])),2)==2)&& freq>200
                error('Frequency should not higher than 200 Hz if three channels in [1:4] or [5:8] are active!');
            elseif (size(find(ismember(obj.activeChannels,[1:4])),2)==1 || size(find(ismember(obj.activeChannels,[5:8])),2)==1) && freq>400
                error('Frequency should not higher than 400 Hz even for a single channel being active!');
            elseif (size(find(ismember(obj.activeChannels,[1:4])),2)==1 && size(find(ismember(obj.activeChannels,[5:8])),2)==1) && freq>333
                error('Frequency should not higher than 333 Hz if one channel in [1:4] and one in [5:8] are active!');
            else
                obj.currentFreq=freq;
                [StopPar,InitPar,UpdateCmd]=obj.SendCommand('Freq');
            end
        end
        
        function SetPW( obj, channel, PW )
            obj.currentPWAll=obj.previousPWAll;
            obj.currentPWAll(channel)=PW;
            obj.SendCommand('PW');
        end
        
        function SetPWAll( obj, PWAll )
            obj.currentPWAll=PWAll;
            obj.SendCommand('PWAll');
        end
        
        function delete( obj )
            % check if the port object is valid and ,if so, is it open
            if isempty( obj.port ), return, end
            if isvalid(obj.port)
                if ~isempty( obj.port ) && strcmp( obj.port.Status, 'open' )
                    fclose( obj.port );
                end
                delete( obj.port );
            end
        end
    end
    
    methods (Access = private)
        function [StopPar,InitPar,UpdatePar]=SendCommand( obj, cmd )
            StopPar=0;
            InitPar=0;
            UpdatePar=0;
            switch cmd
                case 'Freq'
                    if ~isempty(obj.activeChannels)
                        obj.currentAmpAll=obj.previousAmpAll;
                        obj.currentPWAll=obj.previousPWAll;
                        testfwrite1=1;
                        tic
                        while testfwrite1==1
                            try
                                fwrite( obj.port, bin2dec( '11000000' ), 'uint8' );
                                StopPar = fread(obj.port, 1, 'uint8');
                                testfwrite1=0;
                            catch
                            end
                        end
                        t_stop1=toc;
                        cmdInit  = RehaStim_Init_ChannelBased(obj.currentFreq,obj.activeChannels);
                        testfwrite2=1;
                        tic;
                        while testfwrite2==1
                            try
                                fwrite( obj.port, cmdInit, 'uint8' );
                                InitPar = fread(obj.port, 1, 'uint8');
                                
                                testfwrite2=0;
                                
                            catch
                            end
                        end
                        t_init=toc;
                    end
                case {'Amp','AmpAll'}
                    obj.currentFreq=obj.previousFreq;
                    obj.currentPWAll=obj.previousPWAll;
                case {'PW','PWAll'}
                    obj.currentFreq=obj.previousFreq;
                    obj.currentAmpAll=obj.previousAmpAll; 
            end
            obj.previousFreq=obj.currentFreq;
            obj.previousAmpAll=obj.currentAmpAll;
            obj.previousPWAll=obj.currentPWAll;
            if obj.currentAmpAll > obj.MAX_AMP
                obj.currentAmpAll = 2;
                h = warndlg('The chosen amplitude was too high. It was reset to 2 mA.');
            end
            if ~isempty(obj.activeChannels)
                cmdUpdate  = RehaStim_Update_ChannelBased(obj.currentPWAll(obj.activeChannels), obj.currentAmpAll(obj.activeChannels), 2,obj.activeChannels);
                testfwrite=1;
                tic;
                while testfwrite==1
                    try
                        fwrite( obj.port, cmdUpdate, 'uint8' );
                        UpdatePar = fread(obj.port, 1, 'uint8');
                        testfwrite=0;
                    catch
                    end
                end
                t_update=toc;
            else
                testfwrite=1;
                tic;
                while testfwrite==1
                    try
                        fwrite( obj.port, bin2dec( '11000000' ), 'uint8' );
                        StopPar = fread(obj.port, 1, 'uint8');
                        
                        testfwrite=0;
                    catch
                        
                    end
                end
                t_stop=toc;
            end
            pause( obj.writeDelay );           
        end
    end 
end

