function varargout = TestStimRehaStim(varargin)
% Meike: 
% Two problems still remain that one should consider during careful use: 
% 1) If you disable a channel, in that time you cant change its parameter - only when it is switched on...
% 2) If you choose a too high frequency due to multiple channels, the
% additional channel that was to be switched on already gets green but does
% not stimulate yet. It will, though, as soon as you disable one of the
% channels that were already switched on before.
% 

% TESTSTIM MATLAB code for TestStimRehaStim.fig
%      TESTSTIM, by itself, creates a new TESTSTIM or raises the existing
%      singleton*.
%
%      H = TESTSTIM returns the handle to a new TESTSTIM or the handle to
%      the existing singleton*.
%
%      TESTSTIM('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TESTSTIM.M with the given input arguments.
%
%      TESTSTIM('Property','Value',...) creates a new TESTSTIM or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TestStimRehaStim_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TestStimRehaStim_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TestStimRehaStim

% Last Modified by GUIDE v2.5 24-Mar-2015 21:48:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @TestStimRehaStim_OpeningFcn, ...
    'gui_OutputFcn',  @TestStimRehaStim_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TestStimRehaStim is made visible.
function TestStimRehaStim_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TestStimRehaStim (see VARARGIN)

% Choose default command line output for TestStimRehaStim
handles.output = hObject;

portNumber = varargin{ 1 };
chParams = [ 20 2 20 0 0 ];
stimParams = [ chParams; chParams; chParams; chParams ];
stimParams = [ stimParams; chParams; chParams; chParams; chParams ];

handles.stimParams = stimParams;
stim = StimulatorRehaStim( portNumber );
handles.stim = stim;
set( handles.tblStimParams, 'Data', stimParams );
handles.rbCH = [ handles.rbCH1 handles.rbCH2 handles.rbCH3 handles.rbCH4 ...
    handles.rbCH5 handles.rbCH6 handles.rbCH7 handles.rbCH8 ];
InitStim( stim, stimParams );
UpdateControls( handles );
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TestStimRehaStim wait for user response (see UIRESUME)
% uiwait(handles.fig);

function InitStim( stim, stimParams )

stim.Connect;
for i = 1:size( stimParams, 1 )
    %SetChannelParams( obj, channel, chParams )
    %stim.SetChannelParamsFromVector( i, stimParams( i, : ) );
    stim.SetChannelParamsFromVector( i, [stimParams( i, 1 ),0,stimParams( i, 3:5 )] );
    
end


% --- Outputs from this function are returned to the command line.
function varargout = TestStimRehaStim_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function txtFreq_Callback(hObject, eventdata, handles) %#ok<*DEFNU>
% hObject    handle to txtFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtFreq as text
%        str2double(get(hObject,'String')) returns contents of txtFreq as a double
value = str2double( get( hObject, 'String' ) );
f = CheckLimits( value, handles.stim.MAX_FREQ, handles.stim.MIN_FREQ );
set( hObject, 'String', num2str( f ) );
%ch = GetActiveChannel( handles );
handles.stim.SetFreq( f );
for ch=1:8
    handles.stimParams( ch, 1 ) = f;
end
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtFreqUp.
function txtFreqUp_Callback(hObject, eventdata, handles) %#ok<*INUSD>
% hObject    handle to txtFreqUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtFreq, 'String' ) );
set( handles.txtFreq, 'String', num2str( value + 1 ) );
txtFreq_Callback(handles.txtFreq, eventdata, handles)

% --- Executes on button press in txtFreqDown.
function txtFreqDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtFreqDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtFreq, 'String' ) );
set( handles.txtFreq, 'String', num2str( value - 1 ) );
txtFreq_Callback(handles.txtFreq, eventdata, handles)


function txtAmp_Callback(hObject, eventdata, handles)
% hObject    handle to txtAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtAmp as text
%        str2double(get(hObject,'String')) returns contents of txtAmp as a double
value = str2double( get( hObject, 'String' ) );
I = CheckLimits( value, handles.stim.MAX_AMP, handles.stim.MIN_AMP );
set( hObject, 'String', num2str( I ) );
ch = GetActiveChannel( handles );
handles.stim.SetAmp( ch, I );
handles.stimParams( ch, 2 ) = I;
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtAmp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtAmpUp.
function txtAmpUp_Callback(hObject, eventdata, handles)
% hObject    handle to txtAmpUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtAmp, 'String' ) );
set( handles.txtAmp, 'String', num2str( value + 2 ) );
txtAmp_Callback(handles.txtAmp, eventdata, handles)

% --- Executes on button press in txtAmpDown.
function txtAmpDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtAmpDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtAmp, 'String' ) );
set( handles.txtAmp, 'String', num2str( value - 2 ) );
txtAmp_Callback(handles.txtAmp, eventdata, handles)


function txtPW_Callback(hObject, eventdata, handles)
% hObject    handle to txtPW (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtPW as text
%        str2double(get(hObject,'String')) returns contents of txtPW as a double
value = str2double( get( hObject, 'String' ) );
pw = CheckLimits( value, handles.stim.MAX_PW, handles.stim.MIN_PW );
set( hObject, 'String', num2str( pw ) );
ch = GetActiveChannel( handles );
handles.stim.SetPW( ch, pw );
handles.stimParams( ch, 3 ) = pw;
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtPW_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtPW (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtPWUp.
function txtPWUp_Callback(hObject, eventdata, handles)
% hObject    handle to txtPWUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtPW, 'String' ) );
set( handles.txtPW, 'String', num2str( value + 10 ) );
txtPW_Callback(handles.txtPW, eventdata, handles)

% --- Executes on button press in txtPWDown.
function txtPWDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtPWDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtPW, 'String' ) );
set( handles.txtPW, 'String', num2str( value - 10 ) );
txtPW_Callback(handles.txtPW, eventdata, handles)


function txtNumPulses_Callback(hObject, eventdata, handles)
% hObject    handle to txtNumPulses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtNumPulses as text
%        str2double(get(hObject,'String')) returns contents of txtNumPulses as a double
value = str2double( get( hObject, 'String' ) );
numPulses = CheckLimits( value, handles.stim.MAX_NUMPULSES, handles.stim.MIN_NUMPULSES );
set( hObject, 'String', num2str( numPulses ) );
ch = GetActiveChannel( handles );
handles.stim.SetNumOfPulses( ch, numPulses );
handles.stimParams( ch, 4 ) = numPulses;
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtNumPulses_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtNumPulses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtNumPulsesUp.
function txtNumPulsesUp_Callback(hObject, eventdata, handles)
% hObject    handle to txtNumPulsesUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtNumPulses, 'String' ) );
set( handles.txtNumPulses, 'String', num2str( value + 100 ) );
txtNumPulses_Callback(handles.txtNumPulses, eventdata, handles)

% --- Executes on button press in txtNumPulsesDown.
function txtNumPulsesDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtNumPulsesDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtNumPulses, 'String' ) );
set( handles.txtNumPulses, 'String', num2str( value - 100 ) );
txtNumPulses_Callback(handles.txtNumPulses, eventdata, handles)


function txtDelay_Callback(hObject, eventdata, handles)
% hObject    handle to txtDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtDelay as text
%        str2double(get(hObject,'String')) returns contents of txtDelay as a double
value = str2double( get( hObject, 'String' ) );
d = CheckLimits( value, handles.stim.MAX_DELAY, handles.stim.MIN_DELAY );
set( hObject, 'String', num2str( d ) );
ch = GetActiveChannel( handles );
handles.stim.SetDelay( ch, d );
handles.stimParams( ch, 5 ) = d;
set( handles.tblStimParams, 'Data', handles.stimParams );
guidata( handles.fig, handles );

% --- Executes during object creation, after setting all properties.
function txtDelay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtDelay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in txtDelayUp.
function txtDelayUp_Callback(hObject, eventdata, handles)
% hObject    handle to txtDelayUp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtDelay, 'String' ) );
set( handles.txtDelay, 'String', num2str( value + 100 ) );
txtDelay_Callback(handles.txtDelay, eventdata, handles)

% --- Executes on button press in txtDelayDown.
function txtDelayDown_Callback(hObject, eventdata, handles)
% hObject    handle to txtDelayDown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double( get( handles.txtDelay, 'String' ) );
set( handles.txtDelay, 'String', num2str( value - 100 ) );
txtDelay_Callback(handles.txtDelay, eventdata, handles)

% --- Executes on button press in tbCH1.
function tbCH1_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH1
SetChannelState( hObject, handles.stim, 1, get(hObject,'Value') )

% --- Executes on button press in tbCH2.
function tbCH2_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH2
SetChannelState( hObject, handles.stim, 2, get(hObject,'Value') )

% --- Executes on button press in tbCH3.
function tbCH3_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH3
SetChannelState( hObject, handles.stim, 3, get(hObject,'Value') )

% --- Executes on button press in tbCH4.
function tbCH4_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH4
SetChannelState( hObject, handles.stim, 4, get(hObject,'Value') )

% --- Executes on button press in tbCH5.
function tbCH5_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH5
SetChannelState( hObject, handles.stim, 5, get(hObject,'Value') )

% --- Executes on button press in tbCH6.
function tbCH6_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH6
SetChannelState( hObject, handles.stim, 6, get(hObject,'Value') )

% --- Executes on button press in tbCH7.
function tbCH7_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH7
SetChannelState( hObject, handles.stim, 7, get(hObject,'Value') )

% --- Executes on button press in tbCH8.
function tbCH8_Callback(hObject, eventdata, handles)
% hObject    handle to tbCH8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tbCH8
SetChannelState( hObject, handles.stim, 8, get(hObject,'Value') )

% --- Executes when user attempts to close fig.
function fig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to fig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete( handles.stim );
% Hint: delete(hObject) closes the figure
delete(hObject);

function ch = GetActiveChannel( handles )

channels = 1:8;
H = [ handles.rbCH1 handles.rbCH2 handles.rbCH3 handles.rbCH4 ...
    handles.rbCH5 handles.rbCH6 handles.rbCH7 handles.rbCH8 ];
ch = channels( logical( cell2mat( get( H, 'Value' ) )' ) );

function val = CheckLimits( val, maxVal, minVal )
if val > maxVal, val = maxVal; end
if val < minVal, val = minVal; end


% --- Executes during object creation, after setting all properties.
function pnlStimParams_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pnlStimParams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes when selected object is changed in pnlStimParams.
function pnlStimParams_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in pnlStimParams
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
UpdateControls( handles )

function SetChannelState( hObject, stim, channel, state )
handles = guidata( hObject );
set( handles.rbCH( channel ), 'Value', state )
if state == 1,
    handles.stim.ChannelON( channel )
    set( hObject, 'BackgroundColor', [ 0 1 0 ] );

    UpdateControls( handles );
    handles.stim.SetFreq( handles.stimParams(channel,1));
    handles.stim.SetAmp( channel,handles.stimParams(channel,2));
    handles.stim.SetPW( channel,handles.stimParams(channel,3));
else
    handles.stim.ChannelOFF( channel )
    set( hObject, 'BackgroundColor', [ 1 0 0 ] );

    handles.stim.SetPW( channel,handles.stimParams(channel,3));
end


function UpdateControls( handles )
ch = GetActiveChannel( handles );
set( handles.txtFreq, 'String', num2str( handles.stimParams( ch, 1 ) ) );
set( handles.txtAmp, 'String', num2str( handles.stimParams( ch, 2 ) ) );
set( handles.txtPW, 'String', num2str( handles.stimParams( ch, 3 ) ) );
set( handles.txtNumPulses, 'String', num2str( handles.stimParams( ch, 4 ) ) );
set( handles.txtDelay, 'String', num2str( handles.stimParams( ch, 5 ) ) );
