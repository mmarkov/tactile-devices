function varargout = FindStimTHexRehaStim(varargin)
% FINDSTIMTHEX MATLAB code for FindStimTHex.fig
%      FINDSTIMTHEX, by itself, creates a new FINDSTIMTHEX or raises the existing
%      singleton*.
%
%      H = FINDSTIMTHEX returns the handle to a new FINDSTIMTHEX or the handle to
%      the existing singleton*.
%
%      FINDSTIMTHEX('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FINDSTIMTHEX.M with the given input arguments.
%
%      FINDSTIMTHEX('Property','Value',...) creates a new FINDSTIMTHEX or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FindStimTHex_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FindStimTHex_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FindStimTHex

% Last Modified by GUIDE v2.5 12-Feb-2014 08:52:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FindStimTHex_OpeningFcn, ...
                   'gui_OutputFcn',  @FindStimTHex_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FindStimTHex is made visible.
function FindStimTHex_OpeningFcn(hObject, eventdata, handles, varargin) %#ok<*INUSL>
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FindStimTHex (see VARARGIN)
global testState mainButton motButton

testState = 0;
if ~isempty( varargin )
    handles.stim = StimulatorRehaStim( varargin{ 1 } );
else
    handles.stim = StimulatorRehaStim( 'COM8' );
end

handles.currIndex = 1;
set( handles.cboChannels, 'Value', 1 );
               
TH = cell( 8, 4 ); TH( :, : ) = { NaN };
set( handles.tblTH, 'Data', TH );

% Choose default command line output for FindStimTHex
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FindStimTHex wait for user response (see UIRESUME)
% uiwait(handles.mainFig);

% --- Outputs from this function are returned to the command line.
function varargout = FindStimTHex_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btnStart.
function btnStart_Callback(hObject, eventdata, handles) %#ok<*DEFNU,*INUSD>
% hObject    handle to btnStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global testState testTimer
if isempty( testState ) || ( testState == 0 )
    beep
    set( [ handles.txtST, handles.txtDT, handles.txtPT, handles.txtMOT ], 'String', 'NaN' );
    pauseDuration = str2double( get( handles.txtPauseBetweenStimuli, 'String' ) );
    stimDuration = str2double( get( handles.txtStimDuration, 'String' ) );
    testTimer = timer( 'Period', stimDuration + pauseDuration, 'ExecutionMode', 'fixedRate', 'StartFcn', { @InitTest, handles }, ...
               'TimerFcn', { @DoTestStep, handles }, 'StopFcn', { @StopTest, handles } );
    setappdata( handles.mainFig, 'pw', [] );
    set( handles.btnStart, 'String', 'SENSATION?' );
    testState = 1;
    start( testTimer );
elseif ( testState == 1 )
    beep
    pw = getappdata( handles.mainFig, 'pw' );
    set( handles.btnStart, 'String', 'DISCOMFORT?' );
    set( handles.txtST, 'String', pw );
    testState = 2;
elseif (testState == 2 );
    beep
    pw = getappdata( handles.mainFig, 'pw' );
    set( handles.btnStart, 'String', 'PAIN?' );
    set( handles.txtDT, 'String', pw );
    testState = 3;   
elseif ( testState == 3 )
    beep
    pw = getappdata( handles.mainFig, 'pw' );
    set( handles.btnStart, 'String', 'START' );
    set( handles.txtPT, 'String', pw );
    stop( testTimer );
    delete( testTimer );
    testState = 0;
    btnUpdate_Callback( [], [], handles )
end

function DoTestStep( obj, evtData, handles )
global testState
pw = getappdata( handles.mainFig, 'pw' );
PWmin = str2double( get( handles.txtPWmin, 'String' ) );
PWmax = str2double( get( handles.txtPWmax, 'String' ) );
channel = get( handles.cboChannels, 'Value' );
if isempty( pw )
    pw = PWmin;
else
    if testState == 1
        dpw = str2double( get( handles.txtdPW1, 'String' ) );
        pw = pw + dpw;
    elseif( testState == 2 ) || ( testState == 3 )
        dpw = str2double( get( handles.txtdPW2, 'String' ) );
        pw = pw + dpw;
    else
        pw = 50;
        dpw = pw;
    end
    if pw > PWmax, 
        stop( obj ); 
        btnStart_Callback( [], [], handles)
        return; 
    else
        pw = RoundPW( pw, dpw );
    end
end;
setappdata( handles.mainFig, 'pw', pw );
if isobject( handles.stim ) && isvalid( handles.stim ), 
    handles.stim.ChannelON( channel );

    handles.stim.SetPW( channel, pw ); 
end
rectangle( 'Position', [ PWmin 0 pw 1 ], 'FaceColor', 'r', 'EdgeColor', 'r', 'Parent', handles.axPWBar );
set( handles.txtCurr, 'String', pw );

function rpw = RoundPW( pw, dpw )
MINPW = 20; MAXPW = 1000;
validPW = MINPW:dpw:MAXPW;
if validPW( end ) < 1000, validPW( end + 1 ) = 1000; end
diffPW = abs( validPW - pw );
[ ~, I ] = min( diffPW );
rpw = validPW( I );

function InitTest( obj, evtData, handles )
set( handles.txtTestON, 'BackgroundColor', 'r' );
set( handles.txtST, 'String', NaN );
set( handles.txtPT, 'String', NaN );
PWmin = str2double( get( handles.txtPWmin, 'String' ) );
PWmax = str2double( get( handles.txtPWmax, 'String' ) );
set( handles.axPWBar, 'XLim', [ PWmin PWmax ], 'YLim', [ 0 1 ] );
rectangle( 'Position', [ PWmin 0 PWmax 1 ], 'FaceColor', 'w', 'EdgeColor', 'w', 'Parent', handles.axPWBar );
StartStim( handles );

function StartStim( handles )
if ~isobject( handles.stim ) || ~isvalid( handles.stim ), return, end
channel = get( handles.cboChannels, 'Value' );
PWmin = str2double( get( handles.txtPWmin, 'String' ) );
f = str2double( get( handles.txtFreq, 'String' ) );
amp = str2double( get( handles.txtAmp, 'String' ) );
amp=2*floor(amp/2)
 stimDuration = str2double( get( handles.txtStimDuration, 'String' ) );
n = stimDuration * f;
handles.stim.Connect;
%handles.stim.TurnON;
handles.stim.ChannelON( channel )
handles.stim.SetFreq(f ); 
handles.stim.SetPW( channel, PWmin );
handles.stim.SetAmp( channel, amp );
%handles.stim.SetDelay( channel, 0 );
%handles.stim.SetNumOfPulses( channel, n );
%handles.stim.ChannelON( channel );

function StopTest( obj, evtData, handles )
set( handles.txtTestON, 'BackgroundColor', 'k' );
StopStim( handles );


function StopStim( handles )
if ~isobject( handles.stim ) || ~isvalid( handles.stim ), return, end
channel = get( handles.cboChannels, 'Value' );
handles.stim.ChannelOFF( channel );
%handles.stim.TurnOFF;
handles.stim.Disconnect;

% --- Executes on button press in btnUpdate.
function btnUpdate_Callback(hObject, eventdata, handles)
% hObject    handle to btnUpdate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = get( handles.tblTH, 'Data' );
PWST = str2double( get( handles.txtST, 'String' ) );
PWPT = str2double( get( handles.txtPT, 'String' ) );
PWDT = str2double( get( handles.txtDT, 'String' ) );
PWMOT = str2double( get( handles.txtMOT, 'String' ) );

channel = get( handles.cboChannels, 'Value' );
data{ channel, 1 } = PWST;
data{ channel, 2 } = PWDT;
data{ channel, 3 } = PWPT;
data{ channel, 4 } = PWMOT;
set( handles.tblTH, 'Data', data );


% --- Executes on button press in btnStop.
function btnStop_Callback(hObject, eventdata, handles)
% hObject    handle to btnStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global testState testTimer

testState = 0;
set( handles.btnStart, 'String', 'START' );
set( handles.txtTestON, 'BackgroundColor', 'k' );
stop( testTimer );
delete( testTimer );





% --- Executes on selection change in cboChannels.
function cboChannels_Callback(hObject, eventdata, handles)
% hObject    handle to cboChannels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns cboChannels contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cboChannels


% --- Executes during object creation, after setting all properties.
function cboChannels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cboChannels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtST_Callback(hObject, eventdata, handles)
% hObject    handle to txtST (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtST as text
%        str2double(get(hObject,'String')) returns contents of txtST as a double


% --- Executes during object creation, after setting all properties.
function txtST_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtST (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtPT_Callback(hObject, eventdata, handles)
% hObject    handle to txtPT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtPT as text
%        str2double(get(hObject,'String')) returns contents of txtPT as a double


% --- Executes during object creation, after setting all properties.
function txtPT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtPT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function txtCurr_Callback(hObject, eventdata, handles)
% hObject    handle to txtCurr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtCurr as text
%        str2double(get(hObject,'String')) returns contents of txtCurr as a double


% --- Executes during object creation, after setting all properties.
function txtCurr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtCurr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtFreq_Callback(hObject, eventdata, handles)
% hObject    handle to txtFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtFreq as text
%        str2double(get(hObject,'String')) returns contents of txtFreq as a double


% --- Executes during object creation, after setting all properties.
function txtFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtPWmin_Callback(hObject, eventdata, handles)
% hObject    handle to txtPWmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtPWmin as text
%        str2double(get(hObject,'String')) returns contents of txtPWmin as a double


% --- Executes during object creation, after setting all properties.
function txtPWmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtPWmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtPWmax_Callback(hObject, eventdata, handles)
% hObject    handle to txtPWmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtPWmax as text
%        str2double(get(hObject,'String')) returns contents of txtPWmax as a double


% --- Executes during object creation, after setting all properties.
function txtPWmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtPWmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnLoad.
function btnLoad_Callback(hObject, eventdata, handles)
% hObject    handle to btnLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fname = uigetfile( '*.txt' );
fid = fopen( fname );

fclose( fid );

% --- Executes on button press in btnSave.
function btnSave_Callback(hObject, eventdata, handles)
% hObject    handle to btnSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = get( handles.tblTH, 'Data' ); 
fname = uiputfile( '*.xls' );
xlswrite( fname, data );


function txtAmp_Callback(hObject, eventdata, handles)
% hObject    handle to txtAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtAmp as text
%        str2double(get(hObject,'String')) returns contents of txtAmp as a double


% --- Executes during object creation, after setting all properties.
function txtAmp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkUsed.
function chkUsed_Callback(hObject, eventdata, handles)
% hObject    handle to chkUsed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkUsed



function txtdPW1_Callback(hObject, eventdata, handles)
% hObject    handle to text8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text8 as text
%        str2double(get(hObject,'String')) returns contents of text8 as a double


% --- Executes during object creation, after setting all properties.
function txtdPW1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtStimDuration_Callback(hObject, eventdata, handles)
% hObject    handle to txtStimDuration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtStimDuration as text
%        str2double(get(hObject,'String')) returns contents of txtStimDuration as a double


% --- Executes during object creation, after setting all properties.
function txtStimDuration_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtStimDuration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnNext.
function btnNext_Callback(hObject, eventdata, handles)
% hObject    handle to btnNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% channel = get( handles.cboChannels, 'Value' );
if isempty( hObject ), handles = guidata( handles.mainFig ); end
handles.currIndex = handles.currIndex + 1;
if handles.currIndex > 8, handles.currIndex = 1; end
set( handles.cboChannels, 'Value', handles.currIndex );
guidata( handles.mainFig, handles );


% --- Executes when user attempts to close mainFig.
function mainFig_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to mainFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);

function txtPauseBetweenStimuli_Callback(hObject, eventdata, handles)
% hObject    handle to txtPauseBetweenStimuli (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtPauseBetweenStimuli as text
%        str2double(get(hObject,'String')) returns contents of txtPauseBetweenStimuli as a double


% --- Executes during object creation, after setting all properties.
function txtPauseBetweenStimuli_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtPauseBetweenStimuli (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtDT_Callback(hObject, eventdata, handles)
% hObject    handle to txtDT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtDT as text
%        str2double(get(hObject,'String')) returns contents of txtDT as a double


% --- Executes during object creation, after setting all properties.
function txtDT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtDT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnMuscleResp.
function btnMuscleResp_Callback(hObject, eventdata, handles)
% hObject    handle to btnMuscleResp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set( handles.txtMOT, 'String', get( handles.txtCurr, 'String' ) );

function txtdPW2_Callback(hObject, eventdata, handles)
% hObject    handle to txtdPW2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtdPW2 as text
%        str2double(get(hObject,'String')) returns contents of txtdPW2 as a double


% --- Executes during object creation, after setting all properties.
function txtdPW2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtdPW2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txtMOT_Callback(hObject, eventdata, handles)
% hObject    handle to txtMOT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtMOT as text
%        str2double(get(hObject,'String')) returns contents of txtMOT as a double


% --- Executes during object creation, after setting all properties.
function txtMOT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtMOT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnClear.
function btnClear_Callback(hObject, eventdata, handles)
% hObject    handle to btnClear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
button = questdlg( 'Are you sure you want to CLEAR the data?' );
if strcmp( button, 'Yes' )
    TH = cell( 8, 4 ); TH( :, : ) = { NaN };
    set( handles.tblTH, 'Data', TH );
end

function txtOrder_Callback(hObject, eventdata, handles)
% hObject    handle to txtOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txtOrder as text
%        str2double(get(hObject,'String')) returns contents of txtOrder as a double


% --- Executes during object creation, after setting all properties.
function txtOrder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txtOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnPrev.
function btnPrev_Callback(hObject, eventdata, handles)
% hObject    handle to btnPrev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isempty( hObject ), handles = guidata( handles.mainFig ); end
handles.currIndex = handles.currIndex - 1;
if handles.currIndex < 1, handles.currIndex = 8; end
set( handles.cboChannels, 'Value', handles.currIndex );
guidata( handles.mainFig, handles );


% --- Executes on button press in rbTopBottom.
function rbTopBottom_Callback(hObject, eventdata, handles)
% hObject    handle to rbTopBottom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbTopBottom


% --- Executes on button press in rbLeftRight.
function rbLeftRight_Callback(hObject, eventdata, handles)
% hObject    handle to rbLeftRight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbLeftRight


% --- Executes when selected object is changed in panELConfig.
function panELConfig_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in panELConfig 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function panELConfig_CreateFcn(hObject, eventdata, handles)
% hObject    handle to panELConfig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in chkShowEL.
function chkShowEL_Callback(hObject, eventdata, handles)
% hObject    handle to chkShowEL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkShowEL
