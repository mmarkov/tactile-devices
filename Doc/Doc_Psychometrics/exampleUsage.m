portName = 'COM1'; % Define a port where the Stimulator is attached

% RUN THE GUI FOR AMPLITUDE PSYCHOMETRICS
TestAM_C2(portName); % C2
TestAM_Reha(portName); % RehaStim

% RUN THE GUI FOR FREQUENCY PSYCHOMETRICS
TestAM_FREQ_C2(portName); % C2
TestAM_FREQ_Reha(portName); % RehaStim