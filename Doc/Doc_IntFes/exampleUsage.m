portName = 'COM1';
%% STIMULATOR CLASS
% Connect to stimulator
stim = intFES( portName );
while true % Try indefinetly to open a COM port
    try
        stim.Connect();
        break;
    end
end
% Setup stimulation parameters
stim.AllChannelsOFF();
stim.TurnON;
% Free COM port
stim.Disconnect;
stim.delete();

%% CALIBRATION GUI
IntfesTH( portName )

%% TEST GUI
% Currently doesnt exist
