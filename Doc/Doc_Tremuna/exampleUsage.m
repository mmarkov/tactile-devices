portName = 'COM1';
%% STIMULATOR CLASS
% Connect to stimulator
stim = tremUNA( portName );
stim.Connect();
% Setup stimulation parameters
stim.AllChannelsOFF();
stim.TurnON;
% Free COM port
stim.Disconnect;
stim.delete();

%% CALIBRATION GUI
FindStimTHex(portName);

%% TEST GUI
TestStim(portName);
