classdef TactorCM < handle
    % Class to handle the Coin Motors Tactors
    
    properties (Constant = true, GetAccess = private)
        baudRate = 115200;
        stopBits = 1;
        dataBits = 8;
        flowControl = 'none';
        parity = 'none'
    end
    
    properties (Constant = true)
        NUM_CHANNELS = 4;
        MAX_GAIN = 255;
        MIN_GAIN = 0;
    end
    
    properties (SetAccess = private)
        port = [];
        portName = 'COM4';
        infTimer = [];
        currentGain = zeros(1,4,'uint8');
        activeTactors = zeros(1,4,'uint8');
    end
    
    methods
        function obj = TactorCM( portName )
            obj.portName = portName;
        end
        
        % deconstructor
        function delete(obj)
            % the individual timer for each tactor is stopped and deleted
            obj.TurnOff(1:4);
            obj.Disconnect();
        end
        
        function setPort(obj, newPortName)
            obj.portName = newPortName;
        end
        
        
        % to access the properties from outside the class
        function param = getProp(obj, type)
            
            switch type
                case 'gain'
                    param = obj.currentGain;
                case 'activeTactors'
                    param = obj.activeTactors;
            end
        end
        
        function Connect( obj )
            obj.port = serial( obj.portName, 'BaudRate', obj.baudRate, 'StopBits', obj.stopBits, ...
                'DataBits', obj.dataBits, 'FlowControl', obj.flowControl, 'Parity', obj.parity);
            fopen( obj.port );
        end
        
        function Disconnect( obj )
            if ~isempty( obj.port ) && isvalid( obj.port ) && ~strcmp(get(obj.port,'Status'), 'closed')
                fclose( obj.port );
                delete( obj.port );
            end
        end
        
        function SetGain ( obj, input_gain)
            if length(input_gain)==4
                for i = 1:length(input_gain)
                    if input_gain(i) < obj.MIN_GAIN || input_gain(i) > obj.MAX_GAIN
                        input_gain(i) = 127;
                        disp(['Warning: The gain for channel ' num2str(i) ' is otside of range. It was reset to 50%.']);
                    end
                end
                obj.currentGain(1, 1:4) = uint8(input_gain);
            else
                disp('Warning: the gain argumnet must be 1x4 vector of uint8 data type');
            end
        end
        
        
        function TurnOn ( obj, whichCh)
            if length(whichCh) <= 4 && max(whichCh) <= 4 && min(whichCh) >= 1
                cmdC = obj.currentGain;
                obj.activeTactors(whichCh) = 1;
                cmdC(~obj.activeTactors(whichCh)) = 0;
                obj.SendCommand(cmdC);
            else
                disp('Warning: the whichCh argument is maxium 1x4 elements long and has values in the range [1,4]');
            end
        end
        
        function TurnOff(obj, whichCh)
             if length(whichCh) <= 4 && max(whichCh) <= 4 && min(whichCh) >= 1
                cmdC = obj.currentGain;
                obj.activeTactors(whichCh) = 0;
                cmdC(~obj.activeTactors(whichCh)) = 0;
                obj.SendCommand(cmdC);
            else
                disp('Warning: the whichCh argument is maxium 1x4 elements long and has values in the range [1,4]');
            end
        end
        
    end
    
    methods (Access = private)
        % this function sends the packet to the USB port
        function SendCommand( obj, cmdT )
            cmdT = uint8(cmdT);
            obj.activeTactors(1,1:4) = uint8(cmdT>0);
            cmdT = [60 4 cmdT 62];
            fwrite(obj.port,cmdT,'uint8');
        end
    end
    
end