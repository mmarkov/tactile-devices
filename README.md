# Tactile devices

A toolbox containing Psychometrics toolbox and MATLAB interface for:
1) IntFES FES stimulator
2) RehaStim FES stimulator
3) TremUna FES stimulator
4) Engineering Acoustics C2/3 Tactors
5) Coin motors from precision microdrives (arduino controlled)