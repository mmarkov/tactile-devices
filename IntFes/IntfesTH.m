function varargout = IntfesTH(varargin)
% INTFESTH MATLAB code for IntfesTH.fig
%      INTFESTH, by itself, creates a new INTFESTH or raises the existing
%      singleton*.
%
%      H = INTFESTH returns the handle to a new INTFESTH or the handle to
%      the existing singleton*.
%
%      INTFESTH('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INTFESTH.M with the given input arguments.
%
%      INTFESTH('Property','Value',...) creates a new INTFESTH or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before IntfesTH_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to IntfesTH_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help IntfesTH

% Last Modified by GUIDE v2.5 29-Jul-2014 11:21:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IntfesTH_OpeningFcn, ...
                   'gui_OutputFcn',  @IntfesTH_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT


% --- Executes just before IntfesTH is made visible.
function IntfesTH_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to IntfesTH (see VARARGIN)
global state electrodes channels increase n TH
state = (-1);
electrodes = [1 2];
channels = 1:16;
increase = 0.2;
n = 1;
TH = zeros(16,4);

tic;
port = 'COM1';
try
    port = varargin{1};
end
handles.stim = intFES(port);
h = msgbox('Please wait while trying to connect...');
while (toc < 10) && ~(h == 1)
    try
        handles.stim.Connect();
        pause(0.25);
        break;
    end
end
delete(h)

handles.stim.SetPWAll(200);
handles.stim.SetFreqAll(50);

% Choose default command line output for IntfesTH
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes IntfesTH wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = IntfesTH_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function editPulsewidth_Callback(hObject, eventdata, handles)
% hObject    handle to editPulsewidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editPulsewidth as text
%        str2double(get(hObject,'String')) returns contents of editPulsewidth as a double
global state

pw = str2double(get(hObject, 'String'));
if ((state == (-1)) && (pw >= 50) && (pw <= 1000))      
    handles.stim.SetPWAll(pw);
end

% --- Executes during object creation, after setting all properties.
function editPulsewidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editPulsewidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editFreq_Callback(hObject, eventdata, handles)
% hObject    handle to editFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFreq as text
%        str2double(get(hObject,'String')) returns contents of editFreq as a double
global state

freq = str2double(get(hObject, 'String'));
if ((state == (-1)) && (freq >= 1) && (freq <=  400))      
    handles.stim.SetFreqAll(freq);
end

% --- Executes during object creation, after setting all properties.
function editFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editLowAmp_Callback(hObject, eventdata, handles)
% hObject    handle to editLowAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editLowAmp as text
%        str2double(get(hObject,'String')) returns contents of editLowAmp as a double


% --- Executes during object creation, after setting all properties.
function editLowAmp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editLowAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editHighAmp_Callback(hObject, eventdata, handles)
% hObject    handle to editHighAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editHighAmp as text
%        str2double(get(hObject,'String')) returns contents of editHighAmp as a double


% --- Executes during object creation, after setting all properties.
function editHighAmp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editHighAmp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnStart.
function btnStart_Callback(hObject, eventdata, handles)
% hObject    handle to btnStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global state electrodes channels increase ch n TH t m

% channels to be checked
    
if (state == (-1))
    disableParameterChange(handles);
    if (length(electrodes) == 2)
        ch = [channels+((electrodes(1)-1) * 16), channels+((electrodes(2)-1) * 16)];
    else
        ch = (channels+((electrodes(1)-1) * 16));
    end
    set(handles.txtMaxChannel, 'String', num2str(length(ch)));
    set(handles.txtCurrentChannel, 'String', '1');
    m = randperm(length(ch));
    state = 0;
end

switch state
    
    %   ~~~~~ START / NEXT ~~~~~ 
    case 0
        
        t = timer('Period', (0.1/increase), 'ExecutionMode', 'fixedRate', 'StartFcn', {@InitTest, handles}, ...
               'TimerFcn', {@DoTestStep, handles}, 'StopFcn', {@StopTest, handles});
        set(handles.btnStart, 'String', 'Low');
        set(handles.txtCurrentChannel, 'String', num2str(n));
        state = 1;
        start(t);
                
    %   ~~~~~ LOW ~~~~~    
    case 1    
                
        set(handles.editLowAmp, 'String', (get(handles.txtIntensity, 'String')));
        set(handles.btnStart, 'String', 'High');
        if (n == length(ch))
            state = 3;
        else
            state = 2;
        end
        
    %   ~~~~~ HIGH ~~~~~    
    case 2
        
        set(handles.editHighAmp, 'String', (get(handles.txtIntensity, 'String')));
        set(handles.btnStart, 'String', 'Next');
        stop(t);
        delete(t);
        state = 0;
        
    %   ~~~~~ ALL CHANNELS DONE ~~~~~    
    case 3
        
        set(handles.editHighAmp, 'String', (get(handles.txtIntensity, 'String')));
        set(handles.btnStart, 'String', 'Start');
        stop(t);
        delete(t);
        set(handles.tableAmp, 'Data', TH);
        state = (-1);
        enableParameterChange(handles);
        
end


function DoTestStep(obj, evtData, handles)
global n ch m
ampAll = zeros(1,16);
amp = (str2double(get(handles.txtIntensity, 'String')) + 0.1);
elec = ceil(ch(m(n)) / 16);
channel = mod(ch(m(n)), 16);
if (channel == 0) 
    channel = 16; 
end
ampAll(channel) = amp*10;
handles.stim.SetAmpAll(ampAll, elec);
set(handles.txtIntensity, 'String', num2str(amp));

function InitTest(obj, evtData, handles)
global ch m n
set(handles.editLowAmp, 'String', '0.0');
set(handles.editHighAmp, 'String', '0.0');
set(handles.txtIntensity, 'String', '0.0');
handles.stim.SetAmpAll(zeros(1,16), 1);
handles.stim.SetAmpAll(zeros(1,16), 2);
handles.stim.SetActivity(ch(m(n)));
handles.stim.TurnON();

function StopTest(obj, evtData, handles)
global n ch TH m
handles.stim.TurnOFF();
elec = ceil(ch(m(n)) / 16);
channel = mod(ch(m(n)), 16);
if (channel == 0) 
    channel = 16; 
end
TH(channel, ((2*elec)-1)) = str2double(get(handles.editLowAmp, 'String'));
TH(channel, (2*elec)) = str2double(get(handles.editHighAmp, 'String'));
n = n+1;


function editElec_Callback(hObject, eventdata, handles)
% hObject    handle to editElec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editElec as text
%        str2double(get(hObject,'String')) returns contents of editElec as a double
global electrodes state

if (state == (-1))
    str = get(hObject,'String');
    strSize = length(str);
    if (strSize > 1)
        electrodes(1) = str2double(str(1));
        electrodes(2) = str2double(str(end));
    else
        electrodes = str2double(str(1));
    end
end

% --- Executes during object creation, after setting all properties.
function editElec_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editElec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editChannel_Callback(hObject, eventdata, handles)
% hObject    handle to editChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editChannel as text
%        str2double(get(hObject,'String')) returns contents of editChannel as a double
global channels state
nrStart = [];
if (state == (-1))
    str = get(hObject,'String');
    strSize = length(str);
    for n = 1:strSize
        if str(n) == ':'
            nrStart = str2double(str(1:(n-1)));
            nrEnd = str2double(str((n+1):end));
        end
    end
    if (isempty(nrStart))
        m = 1;
        for n = 1:strSize
            if str(n) == ' '
                channels(m) = str2double(str(1:(n-1)));
                m = m+1;
            end
        end
    else
        channels = nrStart : nrEnd;
    end
    channels = unique(channels);
end

% --- Executes during object creation, after setting all properties.
function editChannel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in btnStop.
function btnStop_Callback(hObject, eventdata, handles)
% hObject    handle to btnStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global state TH t n
handles.stim.TurnOFF();
state = (-1);
set(handles.tableAmp, 'Data', TH);
set(handles.btnStart, 'String', 'Start');
set(handles.txtIntensity, 'String', '0.0');
set(handles.editLowAmp, 'String', '0.0');
set(handles.editHighAmp, 'String', '0.0');
enableParameterChange(handles);
try
    stop(t);
    delete(t);
end
n = 1;

% --- Executes on button press in btnSave.
function btnSave_Callback(hObject, eventdata, handles)
% hObject    handle to btnSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global TH 
TH = get(handles.tableAmp, 'Data');
uisave('TH', '');

% --- Executes on button press in btnLoad.
function btnLoad_Callback(hObject, eventdata, handles)
% hObject    handle to btnLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global TH
[FileName,PathName] = uigetfile('*.mat', 'Select the MATLAB code file');
path = [PathName, FileName];
s = load(path);
TH = s.TH;
set(handles.tableAmp, 'Data', TH);

% --- Executes on button press in btnTest.
function btnTest_Callback(hObject, eventdata, handles)
% hObject    handle to btnTest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global channels electrodes state TH

if (state == (-1))
    disableParameterChange(handles);
    handles.stim.SetAmpAll((zeros(1,16)),1);
    handles.stim.SetAmpAll((zeros(1,16)),2);
    handles.stim.TurnON();
    set(handles.txtMaxChannel, 'String', num2str((length(electrodes)*length(channels))*2));
    electrodestest = sort(electrodes);
    channelstest = sort(channels);
    nr = 1;
    for j = electrodestest(1):electrodestest(end)
        for k = channelstest(1):channelstest(end)
            if (TH(k,((2*j) - 1)) ~= 0)
                amp = zeros(1,16);
                amp(k) = TH(k,((2*j) - 1))*10;
                handles.stim.SetAmpAll(amp,j);
                handles.stim.SetActivity((k+((j-1)*16)));
                set(handles.txtCurrentChannel, 'String', num2str(nr));
                nr = nr+1;
                pause(1);
            end
        end
    end
    
    handles.stim.SetAmpAll((zeros(1,16)),1);
    handles.stim.SetAmpAll((zeros(1,16)),2);
    pause(2);

    for j = electrodestest(1):electrodestest(end)
        for k = channelstest(1):channelstest(end)
            if (TH(k,(2*j)) ~= 0)
                amp = zeros(1,16);
                amp(k) = TH(k,(2*j))*10;
                handles.stim.SetAmpAll(amp,j);
                handles.stim.SetActivity((k+((j-1)*16)));
                set(handles.txtCurrentChannel, 'String', num2str(nr));
                pause(1);
            end
            nr = nr+1;
        end
    end
    
    handles.stim.TurnOFF();
    handles.stim.SetAmpAll((zeros(1,16)),1);
    handles.stim.SetAmpAll((zeros(1,16)),2);
    handles.stim.SetActivity([]);
    set(handles.txtIntensity, 'String', '0.0');
    set(handles.editLowAmp, 'String', '0.0');
    set(handles.editHighAmp, 'String', '0.0');
    enableParameterChange(handles)
end

function editIncrease_Callback(hObject, eventdata, handles)
% hObject    handle to editIncrease (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editIncrease as text
%        str2double(get(hObject,'String')) returns contents of editIncrease as a double
global state increase

inc = str2double(get(hObject, 'String'));
if ((state == (-1)) && (inc >= 0.05) && (inc <= 0.5))      
    increase = inc;
end

% --- Executes during object creation, after setting all properties.
function editIncrease_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editIncrease (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
try
    handles.stim.Disconnect();
end
try
    delete(hObject);
end



% --- Executes when entered data in editable cell(s) in tableAmp.
function tableAmp_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to tableAmp (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
global TH
TH(eventdata.Indices(1), eventdata.Indices(2)) = str2double(eventdata.EditData);

function disableParameterChange(handles)
set(handles.editPulsewidth, 'Enable', 'inactive');
set(handles.editFreq, 'Enable', 'inactive');
set(handles.editIncrease, 'Enable', 'inactive');
set(handles.editElec, 'Enable', 'inactive');
set(handles.editChannel, 'Enable', 'inactive');

function enableParameterChange(handles)
set(handles.editPulsewidth, 'Enable', 'on');
set(handles.editFreq, 'Enable', 'on');
set(handles.editIncrease, 'Enable', 'on');
set(handles.editElec, 'Enable', 'on');
set(handles.editChannel, 'Enable', 'on');