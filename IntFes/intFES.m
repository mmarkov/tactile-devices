classdef intFES < handle
    %intFES: Helper class for the control of intFES
    %   Functions for connecting to, programming and activating intFES
    
    properties (Constant = true, GetAccess = private)
        baudRate = 115200;
        terminator = 60;
    end
    
    properties (Constant = true)
        NUM_CHANNELS = 16;
        MAX_FREQ = 400;
        MIN_FREQ = 1;
        MAX_PW = 1000;
        MIN_PW = 50;
        MAX_AMP = 100;
        MIN_AMP = 0;
        MIN_DELAY = 0;
        MAX_DELAY = 50000;
        MIN_NUMPULSES = 0;
        MAX_NUMPULSES = 999999;
    end
    
    properties (SetAccess = private, GetAccess = private)
        cmdON = uint8( '>ON<' );
        cmdOFF = uint8( '>OFF<' );
        cmdC = uint8( '>Cf;xxxxxxxxxxxxxxxx<' ); % choose electrode
        cmdC1 = uint8( '>Cf;xxxxxxxxxxxxxxxx<' ); % first electrode
        cmdC2 = uint8( '>Cf;xxxxxxxxxxxxxxxx<' ); % second electrode
        cmdSF = uint8( '>SF;HL<' );
        cmdSW = uint8( '>SW;HL<' );
        cmdSA = uint8( '>SA;xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx<' );
        %         cmdN = uint8( '>Nn;HL<' );
        %         cmdD = uint8( '>Dn;HL<' );        
    end
    properties (SetAccess = private)
        port = [];
        portName = 'COM40';
        status = [];
    end
    
    methods
        function obj = intFES( portName )
            obj.portName = portName;
        end
        function Connect( obj )
            obj.port = serial( obj.portName, 'BaudRate', obj.baudRate,...
                'terminator', obj.terminator);
            fopen( obj.port );
            pause(1);
            obj.AllChannelsOFF();
        end
        function Disconnect( obj )
            fclose( obj.port );
            delete( obj.port );
        end
        function TurnON( obj )
            obj.SendCommand( obj.cmdON );
        end
        function TurnOFF( obj )
            obj.SendCommand( obj.cmdOFF );
        end
        
        function SetAmpAll( obj, amp, electrode )
            amp( amp>100 ) = 100;
            amp( amp<0 ) = 0;
            electrode( electrode<1 ) = 1;
            electrode( electrode>4 ) = 4;
            if length( amp ) > 16
                amp = amp(1:16);
            end
            if length( amp )<16
                amp = [ amp zeros(1, 16-length( amp )) ];
            end
            if length( electrode ) ~= 1
                electrode = electrode(1);
            end
            obj.cmdC(3) = electrode;
            obj.cmdC( 5:(end-1) ) = amp;
            obj.SendCommand( obj.cmdC );
        end
        
        function SetFreqAll( obj, FREQ )
            [ high, low ] = intFES.GetBytes( FREQ );
            obj.cmdSF( 5 ) = high;
            obj.cmdSF( 6 ) = low;
            obj.SendCommand( obj.cmdSF );
        end
        
        function SetPWAll( obj, PW )
            [ high, low ] = intFES.GetBytes( PW );
            obj.cmdSW( 5 ) = high;
            obj.cmdSW( 6 ) = low;
            obj.SendCommand( obj.cmdSW );
        end
        
        function SetActivity( obj, activeCh )
            obj.cmdSA( 5 : (end - 1) ) = uint8(255);
            if ~isempty(activeCh)
                activeCh = unique(activeCh);
                for i = 1:length(activeCh)
                    electrode = ceil(activeCh(i)/16) - 1;
                    chNumber = activeCh(i) - (electrode*16) - 1;
                    obj.cmdSA( 5 + (i-1) )  = obj.CreateByte( electrode , chNumber);
                end
            end
            obj.SendCommand( obj.cmdSA );
        end
        
        function AllChannelsOFF( obj )
           obj.SetActivity([]);
        end
        
        function SetAmpBoth( obj, amp )
                
            amp( amp>50 ) = 50;
            amp( amp<0 ) = 0;
            if length( amp ) > 32
                amp = amp(1:32);
            end
            if length( amp )<32
                amp = [ amp zeros(1, 32-length( amp )) ];
            end            
            obj.cmdC1( 3 ) = 1;
            obj.cmdC1( 5:(end-1) ) = amp(1:16);
            
            obj.cmdC2( 3 ) = 2;
            obj.cmdC2( 5:(end-1) ) = amp(17:32);
            
            obj.SendCommand( [obj.cmdC1 obj.cmdC2] );
        end
        
        function SetActivityAmp(obj, activeCh, amp )
                        
            obj.cmdSA( 5 : (end - 1) ) = uint8(255);
            if ~isempty(activeCh)
                activeCh = unique(activeCh);
                for i = 1:length(activeCh)
                    electrode = ceil(activeCh(i)/16) - 1;
                    chNumber = activeCh(i) - (electrode*16) - 1;
                    obj.cmdSA( 5 + (i-1) )  = obj.CreateByte( electrode , chNumber);
                end
            end
            
            amp( amp>50 ) = 50;
            amp( amp<0 ) = 0;
            if length( amp ) > 32
                amp = amp(1:32);
            end
            if length( amp )<32
                amp = [ amp zeros(1, 32-length( amp )) ];
            end
            
            obj.cmdC1( 3 ) = 1;
            obj.cmdC1( 5:(end-1) ) = amp(1:16);
            
            obj.cmdC2( 3 ) = 2;
            obj.cmdC2( 5:(end-1) ) = amp(17:32);
            
            obj.SendCommand( [obj.cmdSA obj.cmdC1 obj.cmdC2] );
        end
        
        function SetActAmpFreq(obj, FREQ, activeCh, amp)
              
            [ high, low ] = intFES.GetBytes( FREQ );
            obj.cmdSF( 5 ) = high;
            obj.cmdSF( 6 ) = low;
            
            obj.cmdSA( 5 : (end - 1) ) = uint8(255);
            if ~isempty(activeCh)
                activeCh = unique(activeCh);
                for i = 1:length(activeCh)
                    electrode = ceil(activeCh(i)/16) - 1;
                    chNumber = activeCh(i) - (electrode*16) - 1;
                    obj.cmdSA( 5 + (i-1) )  = obj.CreateByte( electrode , chNumber);
                end
            end
            
            amp( amp>50 ) = 50;
            amp( amp<0 ) = 0;
            if length( amp ) > 32
                amp = amp(1:32);
            end
            if length( amp )<32
                amp = [ amp zeros(1, 32-length( amp )) ];
            end
            
            obj.cmdC1( 3 ) = 1;
            obj.cmdC1( 5:(end-1) ) = amp(1:16);
            
            obj.cmdC2( 3 ) = 2;
            obj.cmdC2( 5:(end-1) ) = amp(17:32);
            
            obj.SendCommand( [obj.cmdSF obj.cmdSA obj.cmdC1 obj.cmdC2] );
        end
        
        function SetActFreq( obj, FREQ, activeCh )
              
            [ high, low ] = intFES.GetBytes( FREQ );
            obj.cmdSF( 5 ) = high;
            obj.cmdSF( 6 ) = low;
            
            obj.cmdSA( 5 : (end - 1) ) = uint8(255);
            if ~isempty(activeCh)
                activeCh = unique(activeCh);
                for i = 1:length(activeCh)
                    electrode = ceil(activeCh(i)/16) - 1;
                    chNumber = activeCh(i) - (electrode*16) - 1;
                    obj.cmdSA( 5 + (i-1) )  = obj.CreateByte( electrode , chNumber);
                end
            end
            
            obj.SendCommand( [obj.cmdSF obj.cmdSA] );
        end
        
        function SetAll(obj, FREQ, PW, activeCh, amp)
            [ high, low ] = intFES.GetBytes( PW );
            obj.cmdSW( 5 ) = high;
            obj.cmdSW( 6 ) = low;
            
            [ high, low ] = intFES.GetBytes( FREQ );
            obj.cmdSF( 5 ) = high;
            obj.cmdSF( 6 ) = low;
            
            obj.cmdSA( 5 : (end - 1) ) = uint8(255);
            if ~isempty(activeCh)
                activeCh = unique(activeCh);
                for i = 1:length(activeCh)
                    electrode = floor(activeCh(i)/16);
                    chNumber = activeCh(i) - (electrode*16) - 1;
                    obj.cmdSA( 5 + (i-1) )  = obj.CreateByte( electrode , chNumber);
                end
            end
            
            amp( amp>100 ) = 100;
            amp( amp<0 ) = 0;
            if length( amp ) > 16
                amp = amp(1:16);
            end
            if length( amp )<16
                amp = [ amp zeros(1, 16-length( amp )) ];
            end
            obj.cmdC( 3 ) = 1;
            obj.cmdC( 5:(end-1) ) = amp;
            obj.SendCommand( [obj.cmdSW obj.cmdSF obj.cmdSA obj.cmdC] );
        end
        
        function delete( obj )
            % check if the port object is valid and ,if so, is it open
            if isempty( obj.port ), return, end
            if isvalid( obj.port )
                if ~isempty( obj.port ) && strcmp( obj.port.Status, 'open' )
                    fclose( obj.port );
                end
                delete( obj.port );
            end
        end
    end
    methods (Static, Access = private)
        function [ high, low ] = GetBytes( w )
            low = bitand( uint16( w ), hex2dec( 'FF' ) );
            high = bitshift( bitand( uint16( w ), hex2dec( 'FF00' ) ), -8 );
        end
        function byte = CreateByte( wH, wL )
            byte = bitor( bitshift( uint8(wH) , 4 ), uint8( wL ) );
        end
    end
    methods (Access = private)
        function SendCommand( obj, cmd )
            fwrite( obj.port, cmd);
        end
    end
end

