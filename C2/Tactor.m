classdef Tactor < handle
    % Class to handle the C2 Tactors
    
    properties (Constant = true, GetAccess = private)
        baudRate = 115500;
        stopBits = 1;
        dataBits = 8;
        flowControl = 'none';
        parity = 'none'
    end
    
    properties (Constant = true)
        NUM_CHANNELS = 8;
        MAX_FREQ = 3000;
        MIN_FREQ = 300;
        MAX_GAIN = 255;
        MIN_GAIN = 1;
        MIN_RUNTIME = 1;
        MAX_RUNTIME = 255;
    end
    
    properties (SetAccess = private)
        port = [];
        portName = 'COM4';
        infTimer = [];
    end
    
    properties (SetAccess = private, SetObservable = true)
        prop = cell(8,4);
    end
    
    methods
        function obj = Tactor( portName )
            obj.portName = portName;
            % prop contains all properties of the Tactor object: gain (1st
            % column), frequency (2nd column), current state (3rd column).
            % The duration of its last firing can be accessed through the
            % Tactor's timer (4th column)
            obj.prop(:,1) = {obj.MIN_GAIN};
            obj.prop(:,2) = {obj.MIN_FREQ};
            obj.prop(:,3) = {0};
            % infTimer handles the "infinite turning on" of all Tactors, by
            % resending the turn on command every 5 ms
            obj.infTimer = timer('Period', 0.05, 'BusyMode', 'drop','ExecutionMode','fixedRate',...
                'TimerFcn', @(src, event)Tactor.TurnOnCallback(src, event, obj, 50),...
                'TasksToExecute', inf);
            % the timers for each tactor is initialized and stored in the
            % 4th column of prop
            for i = 1:length(obj.prop)
                obj.prop(i,4) = {timer('Tag',num2str(i),'TimerFcn',@(src, event)Tactor.StopTimer(src, event, obj))};
            end
        end
        
        % deconstructor
        function delete(obj)
            % the individual timer for each tactor is stopped and deleted
            for i = 1:length(obj.prop)
                stop(obj.prop{i,4});
                delete(obj.prop{i,4});
            end
            stop(obj.infTimer);
            delete(obj.infTimer);
            for i = 1:length(obj.prop)
                delete(obj.prop{i,4});
            end
            obj.Disconnect();
        end
        
        function setPort(obj, newPortName)
            obj.portName = newPortName;
        end
        
        
        % to access the properties from outside the class
        function param = getProp(obj, type)
            switch type
                case 'all'
                    param = obj.prop;
                otherwise
                    param = zeros(8,1);
            end
            switch type
                case 'all'
                    return
                case 'gain'
                    param = obj.prop{:,1};
                case 'freq'
                    param = obj.prop{:,2};
                case 'cstate'
                    param = obj.prop{:,3};
                case 'time'
                    for i = 1:8
                        param(i) = obj.prop{i,4}.StartDelay;
                        param(obj.prop{i,3} == inf) = inf;
                    end
            end
        end
        
        function Connect( obj )
            obj.port = serial( obj.portName, 'BaudRate', obj.baudRate, 'StopBits', obj.stopBits, ...
                'DataBits', obj.dataBits, 'FlowControl', obj.flowControl, 'Parity', obj.parity);
            fopen( obj.port );
        end
        
        function Disconnect( obj )
            if ~isempty( obj.port ) && isvalid( obj.port ) && ~strcmp(get(obj.port,'Status'), 'closed')
                fclose( obj.port );
                delete( obj.port );
            end
        end
        
        function SetGain ( obj, channel, input_gain)
            % the gain has to be in the range of 1 to 255
            if length(input_gain) ~= length(channel)
                input_gain = input_gain * ones(length(channel), 1);
            end
            setProp(obj, channel, input_gain, 1);
            for i = 1:length(channel)
                if input_gain(i) > obj.MAX_GAIN
                    input_gain(i) = obj.MAX_GAIN;
                    disp(['Warning: The gain for channel ' num2str(i) ' is too high. It was reset to the maximum parameter (255).']);
                end
                if input_gain < obj.MIN_GAIN
                    input_gain = obj.MIN_GAIN;
                    disp(['Warning: The gain for channel ' num2str(i) ' is too low. It was reset to the maximum parameter (1).']);
                end
                % convert gain from double to 2-digit hexadecimal
                gain = dec2hex(input_gain(i),2);
                for j=1:length(channel)
                    % the low level command is always coded in the 2-digit
                    % hexadecimal numbers: start of command (always '02'),
                    % delay factor, number of commands in the packet,
                    % number of bytes (coded in two bytes in little-endian
                    % notion, so 4 bytes are coded as '04','00'), the
                    % command byte (see EAI TDK command sheet), delay of
                    % this command (in ms, multiplied by the general delay
                    % factor given in the second byte of the packet), more
                    % bytes, depending on the specific command. At last
                    % the checksum byte (x or connection of all other
                    % bytes), which here is generated by a function and is
                    % automatically attached to the packet.
                    obj.SendCommand(['02';'00';'01';'04';'00';'20';'00';dec2hex(channel(j),2);gain]);
                end
            end
        end
        
        function SetFreq ( obj, channel, freq)
            % the frequency has to be in the range of 30 Hz and 300 Hz
            if length(freq) ~= length(channel)
                freq = freq * ones(length(channel), 1);
            end
            obj.setProp(channel, freq, 2);
            freq = freq * 10;
            for i=1:length(channel)
                if freq(i) > obj.MAX_FREQ;
                    freq(i) = obj.MAX_FREQ;
                    disp(['Warning: The frequency for channel ' num2str(i) ' is too high. It was reset to the maximum parameter (300 Hz).']);
                end
                if freq(i) < obj.MIN_FREQ
                    freq(i) = obj.MIN_FREQ;
                    disp(['Warning: The frequency for channel ' num2str(i) ' is too low. It was reset to the minimum parameter (30 Hz).']);
                end
                % convert frequency from double to two 2-digit hexadecimals
                % in little-endian notion
                freq_char = dec2hex(freq(i),4);
                freq1 = freq_char(3:4);
                freq2 = freq_char(1:2);
                for j=1:length(channel)
                    obj.SendCommand(['02';'00';'01';'05';'00';'12';'00';dec2hex(channel(j),2);freq1;freq2]);
                end
            end
        end
        
        function TurnOn ( obj, channel, time)
            % The time parameter can be either a number between 1 and 2550
            % (ms) or inf.
            if length(time) ~= length(channel)
                time = time * ones(length(channel), 1);
            end
            time = time / 10;
            for i = 1:length(channel)
                if time(i) ~= inf && time(i) > obj.MAX_RUNTIME
                    disp('Warning: Your runtime value was too big. It was reset to the maximum fixed runtime (2550 ms). If youn want to run it longer, use "inf" as a time parameter. The Tactor will then run infinitely until you turn it off with TurnOff(obj, channel)');
                    time(i) = obj.MAX_RUNTIME;
                end
                if time(i) < obj.MIN_RUNTIME
                    time(i) = obj.MIN_RUNTIME;
                    disp('Warning: Your runtime value was too small. It was reset to the minimum fixed runtime (10 ms).');
                end
                % checks if the tactor should be turned on for a specific
                % time or infinitely
                switch true
                    case time(i) == inf
                        obj.prop(channel(i),3) = {inf};
                        if strcmp(obj.infTimer.Running, 'off') == 1
                            start(obj.infTimer);
                        end
                    case time(i) <= obj.MAX_RUNTIME && time(i) >= obj.MIN_RUNTIME
                        % if the tactor is already running, stop it, if the
                        % command is resend
                        if length(obj.prop{channel(i),4}.Running) == 2
                            stop(obj.prop{channel(i),4});
                        end
                        % the current state of the tactor is changed and
                        % will be changed back by it's timer after the time
                        % given by the user
                        obj.prop(channel(i),3) = {1};
                        set(obj.prop{channel(i),4}, 'StartDelay', time(i)/100);
                        start(obj.prop{channel(i),4});
                        if time(i) < 16
                            obj.SendCommand(['02';'00';'01';'04';'00';'11';'00';dec2hex(channel(i),2);dec2hex(time(i),2)]);
                        else
                            obj.SendCommand(['02';'00';'01';'04';'00';'11';'00';dec2hex(channel(i),2);dec2hex(time(i),2)]);
                        end
                end
            end
        end
        
        function TurnOff(obj, channel)
            % checks which channels are active at the moment
            bit_vector = zeros(8,1);
            isactive = cell2mat(obj.prop(:,3))>0;
            bit_vector(isactive) = 1;
            % sets the channels given by the user to zero (those will be
            % turned off)
            bit_vector(channel) = 0;
            obj.prop(channel,3) = {0};
            % the bit vecotr indicating which channels should stay turned on
            % and which should be turned off is convered in the right
            % format and data type
            tactor_state_byte_field = binaryVectorToHex(bit_vector','LSBFirst');
            SendCommand(obj,[ '02'; '00'; '01'; '03'; '00'; '80'; '00'; tactor_state_byte_field]);
        end
    end
    
    methods (Access = private)
        
        % to change the class properties
        function setProp(obj, channel, input, type)
            for i = 1:length(channel)
                obj.prop(channel(i),type) = {input(i)};
            end
        end
        
        % this function calculates the checksum (the x or value of all
        % other bytes of the packet
        function checksum = x_or(obj, packet)
            checksum = packet(1);
            for i=1:length(packet)-1
                checksum_tmp = bitxor(checksum,packet(i+1));
                checksum = checksum_tmp;
            end
            % the x or connection with the number 234 has to be done in the
            % end (stated in the EAI TDK command sheet)
            checksum = bitxor(checksum,234);
        end
        
        % this function sends the packet to the USB port
        function SendCommand( obj, packet )
            packet = uint8(hex2dec(packet));
            packet = [packet' x_or(obj,packet)];
            fwrite(obj.port,packet,'uint8');
        end
    end
    
    % this function is called by the individual timers of the tactors and
    % sets the property "current state" (column 3) to 0 again, after the
    % tactor has been on for some time between 1 ms and 2550 ms
    methods (Access = private, Static)
        function StopTimer(src,event,obj)
            if ~isinf(obj.prop{str2double(src.Tag),3})
                obj.prop{str2double(src.Tag),3} = 0;
            end
        end
        
        % this functions repeatedly turns on the tactor so that it runs
        % infinitely
        function TurnOnCallback (src, event, obj, time)
            if isequal(obj.prop{:,3},{0;0;0;0;0;0;0;0})
                stop(obj.infTimer);
            end
            for i = 1:length(obj.prop)
                if isinf(obj.prop{i,3})
                    obj.SendCommand(['02';'00';'01';'04';'00';'11';'00';dec2hex(i,2);dec2hex(time,2)]);
                end
            end
        end
    end
end