function varargout = Tactor_GUI2(varargin)
% TACTOR_GUI2 MATLAB code for Tactor_GUI2.fig
%      TACTOR_GUI2, by itself, creates a new TACTOR_GUI2 or raises the existing
%      singleton*.
%
%      H = TACTOR_GUI2 returns the handle to a new TACTOR_GUI2 or the handle to
%      the existing singleton*.
%
%      TACTOR_GUI2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TACTOR_GUI2.M with the given input arguments.
%
%      TACTOR_GUI2('Property','Value',...) creates a new TACTOR_GUI2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Tactor_GUI2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Tactor_GUI2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Tactor_GUI2

% Last Modified by GUIDE v2.5 08-Dec-2015 14:13:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Tactor_GUI2_OpeningFcn, ...
    'gui_OutputFcn',  @Tactor_GUI2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Tactor_GUI2 is made visible.
function Tactor_GUI2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Tactor_GUI2 (see VARARGIN)

% Choose default command line output for Tactor_GUI2
handles.output = hObject;
handles.tac = Tactor('');
handles.unclicked = true;
lh = addlistener(handles.tac, 'prop', 'PostSet', @(src,event)lhcallback(src,event,handles));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Tactor_GUI2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Tactor_GUI2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in connect_button.
function connect_button_Callback(hObject, eventdata, handles)
% hObject    handle to connect_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%strrep(

if strcmp(get(handles.connect_button,'String'),'Connect to') && handles.unclicked
    h = warndlg('Connecting. Please wait.');
    contents = cellstr(get(handles.PortPopup,'String'));
    setPort(handles.tac,contents{get(handles.PortPopup,'Value')});
    Connect(handles.tac);
    for i = 1:8
            set(handles.(['parameter_panel' num2str(i)]), 'Visible','on');
    end
    set(handles.PortPopup, 'visible', 'off');
    set(handles.connect_button,'String','Disconnect');
    handles.unclicked = false;
    delete(h);
end
if strcmp(get(handles.connect_button,'String'),'Disconnect') && handles.unclicked
    handles.tac.TurnOff([1,2,3,4,5,6,7,8]);
    handles.tac.Disconnect;
    for i = 1:8
            set(handles.(['parameter_panel' num2str(i)]), 'Visible','off');
    end
    set(handles.PortPopup, 'visible', 'on');
    set(handles.connect_button,'String','Connect to');
    handles.unclicked = false;
end
handles.unclicked = true;

% --- Executes during object creation, after setting all properties.
function PortPopup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PortPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
string_list = {};
for i = 1:40
    string_list = [string_list; ['COM' num2str(i)]];
end
set(hObject,'String',string_list);


% --- Executes on selection change in PortPopup.
function PortPopup_Callback(hObject, eventdata, handles)
% hObject    handle to PortPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PortPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PortPopup

function group_panel_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in group_panel2 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
handleTag = get(eventdata.Source, 'Tag');
tagN = handleTag(end);
switch eventdata.NewValue.String
    case 'ON'
        handles.tac.SetGain(str2num(tagN), round(get(handles.(['gain_slider' tagN]),'Value')));
        handles.tac.SetFreq(str2num(tagN), round(get(handles.(['freq_slider' tagN]),'Value')));
        handles.tac.TurnOn(str2num(tagN), inf);
    case 'OFF'
        handles.tac.TurnOff(str2num(tagN));               
end

% --- Executes on slider movement.
function freq_slider_Callback(hObject, eventdata, handles)
% hObject    handle to freq_slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handleTag = get(hObject, 'Tag');
tagN = handleTag(end);
handles.tac.SetFreq(str2num(tagN), round(get(hObject,'Value')));
set(handles.(['freq_int' tagN]),'String',[int2str(round(get(hObject,'Value'))) ' Hz']);

% --- Executes on slider movement.
function gain_slider_Callback(hObject, eventdata, handles)
% hObject    handle to freq_slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handleTag = get(hObject, 'Tag');
tagN = handleTag(end);
handles.tac.SetGain(str2num(tagN), round(get(hObject,'Value')));
set(handles.(['gain_int' tagN]),'String',[int2str(round(get(hObject,'Value')/255*100)) ' %']);

% --- Executes on button press in start_button1.
function start_button_Callback(hObject, eventdata, handles)
% hObject    handle to start_button1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handleTag = get(hObject, 'Tag');
tagN = handleTag(end);
handles.tac.SetGain(str2num(tagN), round(get(handles.(['gain_slider' tagN]),'Value')));
handles.tac.SetFreq(str2num(tagN), round(get(handles.(['freq_slider' tagN]),'Value')));
handles.tac.TurnOn(str2num(tagN), str2double(get(handles.(['time_window' tagN]),'String')));



%% Tactor 1
% --- Executes during object creation, after setting all properties.
function time_window1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_window1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in start_button1.
function start_button1_Callback(hObject, eventdata, handles)
% hObject    handle to start_button1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.tac.SetGain(1, round(get(handles.gain_slider1,'Value')));
handles.tac.SetFreq(1, round(get(handles.freq_slider1,'Value')));
handles.tac.TurnOn(1, str2double(get(handles.time_window1,'String')));


% --- Executes during object creation, after setting all properties.
function freq_slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function gain_slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gain_slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%% Tactor 2
% --- Executes during object creation, after setting all properties.
function gain_slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gain_slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function time_window2_Callback(hObject, eventdata, handles)
% hObject    handle to time_window2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_window2 as text
%        str2double(get(hObject,'String')) returns contents of time_window2 as a double

% --- Executes during object creation, after setting all properties.
function time_window2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_window2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in start_button2.
function start_button2_Callback(hObject, eventdata, handles)
% hObject    handle to start_button2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.tac.SetGain(2, round(get(handles.gain_slider2,'Value')));
handles.tac.SetFreq(2, round(get(handles.freq_slider2,'Value')));
handles.tac.TurnOn(2, str2double(get(handles.time_window2,'String')));

%% Tactor 3
% --- Executes on button press in start_button3.
function start_button3_Callback(hObject, eventdata, handles)
% hObject    handle to start_button3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.tac.SetGain(3, round(get(handles.gain_slider3,'Value')));
handles.tac.SetFreq(3, round(get(handles.freq_slider3,'Value')));
handles.tac.TurnOn(3, str2double(get(handles.time_window3,'String')));

% --- Executes during object creation, after setting all properties.
function time_window3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_window3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function time_window3_Callback(hObject, eventdata, handles)
% hObject    handle to time_window3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_window3 as text
%        str2double(get(hObject,'String')) returns contents of time_window3 as a double

% --- Executes during object creation, after setting all properties.
function gain_slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gain_slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function freq_slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function start_button2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to start_button2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function freq_slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
% handles.tac.TurnOff(1:8);
handles.tac.delete();
delete(hObject);

function lhcallback(~,event,handles)
for i = 1:8
    if event.AffectedObject.prop{i,3} ~= 0
        set(handles.(['status_light' num2str(i)]), 'BackgroundColor','green');
    else
        set(handles.(['status_light' num2str(i)]), 'BackgroundColor','red');
    end
end



function time_window4_Callback(hObject, eventdata, handles)
% hObject    handle to time_window4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_window4 as text
%        str2double(get(hObject,'String')) returns contents of time_window4 as a double


% --- Executes during object creation, after setting all properties.
function time_window4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_window4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start_button4.
function start_button4_Callback(hObject, eventdata, handles)
% hObject    handle to start_button4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Executes during object creation, after setting all properties.
function freq_slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function gain_slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gain_slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function slider13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function time_window5_Callback(hObject, eventdata, handles)
% hObject    handle to time_window5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_window5 as text
%        str2double(get(hObject,'String')) returns contents of time_window5 as a double


% --- Executes during object creation, after setting all properties.
function time_window5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_window5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start_button5.
function start_button5_Callback(hObject, eventdata, handles)
% hObject    handle to start_button5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function gain_slider5_Callback(hObject, eventdata, handles)
% hObject    handle to gain_slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function gain_slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gain_slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function freq_slider5_Callback(hObject, eventdata, handles)
% hObject    handle to freq_slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function freq_slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes when selected object is changed in group_panel5.
function group_panel5_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in group_panel5 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function time_window6_Callback(hObject, eventdata, handles)
% hObject    handle to time_window6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_window6 as text
%        str2double(get(hObject,'String')) returns contents of time_window6 as a double


% --- Executes during object creation, after setting all properties.
function time_window6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_window6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start_button6.
function start_button6_Callback(hObject, eventdata, handles)
% hObject    handle to start_button6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function gain_slider6_Callback(hObject, eventdata, handles)
% hObject    handle to gain_slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function gain_slider6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gain_slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function freq_slider6_Callback(hObject, eventdata, handles)
% hObject    handle to freq_slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function freq_slider6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes when selected object is changed in group_panel6.
function group_panel6_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in group_panel6 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function time_window7_Callback(hObject, eventdata, handles)
% hObject    handle to time_window7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_window7 as text
%        str2double(get(hObject,'String')) returns contents of time_window7 as a double


% --- Executes during object creation, after setting all properties.
function time_window7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_window7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start_button7.
function start_button7_Callback(hObject, eventdata, handles)
% hObject    handle to start_button7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function gain_slider7_Callback(hObject, eventdata, handles)
% hObject    handle to gain_slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function gain_slider7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gain_slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function freq_slider7_Callback(hObject, eventdata, handles)
% hObject    handle to freq_slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function freq_slider7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes when selected object is changed in group_panel7.
function group_panel7_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in group_panel7 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function time_window8_Callback(hObject, eventdata, handles)
% hObject    handle to time_window8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of time_window8 as text
%        str2double(get(hObject,'String')) returns contents of time_window8 as a double


% --- Executes during object creation, after setting all properties.
function time_window8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to time_window8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start_button8.
function start_button8_Callback(hObject, eventdata, handles)
% hObject    handle to start_button8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function gain_slider8_Callback(hObject, eventdata, handles)
% hObject    handle to gain_slider8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function gain_slider8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gain_slider8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function freq_slider8_Callback(hObject, eventdata, handles)
% hObject    handle to freq_slider8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function freq_slider8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_slider8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes when selected object is changed in group_panel8.
function group_panel8_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in group_panel8 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
