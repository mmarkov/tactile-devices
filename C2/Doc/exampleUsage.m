% Some example commands for using the Tactor class

% the only parameter the constructor needs is the USB port to which the
% device is connected to
t = Tactor('COM24');

% this command sets up the connection to the port
% afterwards you are ready to send commands
t.Connect();

% this command sets the gain (the amplitude, second parameter) of the Tactors
% you can set the gain for one or several Tactors (channels, first parameter)
% if you provide several channels, but only one gain parameter, this will
% be applied to all channels
% if you don't set the gain, the gain will be the same that was last set
% (at previous use)
% the gain has to be in the range of 1 to 255
t.SetGain(2,100);
t.SetGain([1 2 3], [100 150 200]);
t.SetGain([1 2 3], 150);

% the same goes for the frequency
% the frequency has to be in the range of 30 Hz and 250 Hz
t.SetFreq(2,30);
t.SetFreq([1 2 3], [88 30 250]);
t.SetFreq([1 2 3], 150);

% the same goes for the time parameter
% The time parameter can be either a number between 1 and 2550
% (ms) or for an infinite amount of time (put inf as a parameter).
t.TurnOn(2,300);
t.TurnOn([1 2 3], [1000 2000 500]);
t.TurnOn([1 2 3], inf);

% with this command, you can turn an individual Tacor (or some or all of
% them off)
t.TurnOff(2);
t.TurnOff([1 3]);

% with this command you can read the different properties of the Tactors.
% A vector with the respective values for all 8 Tactors will be returned 
% with ('gain', 'freq', 'cstate' or 'time') as a parameter
% the complete propertys in a structure array (with 'all' as a parameter)
disp(t.getProp('gain'));
disp(t.getProp('freq'));
% the 'active' parameter gives a vector with numbers indicating which
% tactors are on (inf or 1) or off (0)
disp(t.getProp('cstate'));
% the 'time' parameter gives the time in seconds that the Tactor was last
% active
disp(t.getProp('time'));
% gives back a cell array with the gain (1st column), frequency (2md
% column), current state (3rd column) and timer (4th column) for each
% tactor
prop = t.getProp('all');

% when you are finished, you need to disconnect from the port with this
% command
t.Disconnect();

% the deconstructor deletes the object and all class variables
t.delete();